#include <stdio.h>
#include <stdlib.h>

int foo(int bar)
{
    printf("In foo(%d)\n", bar);

    return bar;
}

int main()
{
    int (*fun)(int) = foo;

    int bar = 14;
    int bar2 = fun(bar);

    printf("bar2 = %d\n", bar2);

    return EXIT_SUCCESS;
}
