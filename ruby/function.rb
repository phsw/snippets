def contains_not_contains(str, contains, not_contains)
  for c in contains
    if not str.include?(c)
      return false
    end
  end

  for c in not_contains
    if str.include?(c)
      return false
    end
  end

  return true
end

puts contains_not_contains("truc bar", ["toto", "bar"], ["foo", "truc"])
