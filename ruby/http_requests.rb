require 'net/http'
require 'tempfile'
require 'uri'

response = Net::HTTP.get_response("localhost", "/vde/index.php")
file = Tempfile.new()
puts file.path
file.write(response.body)
file.close

response = Net::HTTP.post_form(URI("http://localhost/vde/add_vde.php"), { "pseudo" => "toto", "content" => "une histoire" })
puts response.code
case response
when Net::HTTPRedirection then
  puts response["location"]
end
