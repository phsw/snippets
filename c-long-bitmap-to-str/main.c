#include <stdlib.h>
#include <stdio.h>
#include <string.h>


void convert_numa_nodes_bitmap_to_str(long bitmap, char* str)
{
	if (bitmap < 0)
	{
		sprintf(str, "%ld", bitmap);
	}
	else
	{
		long i = 0;
		int first = 1;
		for (; i < (long) (sizeof(bitmap)*8); i++)
		{
			printf("%u: (%lx) %ld\n", i, (long) 1<<i, bitmap & ((long) 1 << i));
			if (bitmap & ((long) 1 << i))
			{
				if (first)
				{
					sprintf(str, "%lu", i);
					first = 0;
				}
				else
				{
					strcat(str, ",");
					char number[4];
					sprintf(number, "%lu", i);
					strcat(str, number);
				}
			}
		}
	}

	printf("%ld (%lx) -> %s\n", bitmap, bitmap, str);
}

int main()
{
	long v = 4;
	char str[15];

	convert_numa_nodes_bitmap_to_str(v, str);

	return EXIT_SUCCESS;
}
