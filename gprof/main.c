#include <stdlib.h>
#include <stdio.h>

#define ARRAY_SIZE 1024
#define DEFAULT_ITER 100000

#if 0
__attribute__((noinline))
#endif
void* mymalloc(size_t size)
{
    return malloc(size);
}

void init_array(int* array, int size)
{
    for (int i = 0; i < size; i++)
    {
        array[i] = i;
    }
}

long sum_array(int* array, int size)
{
    long s = 0;
    for (int i = 0; i < size; i++)
    {
        s += array[i];
    }
    return s;
}

int foo()
{
    int ok = 1;
    int* array = mymalloc(ARRAY_SIZE*sizeof(int));
    init_array(array, ARRAY_SIZE);

    if (sum_array(array, ARRAY_SIZE) != ARRAY_SIZE*(ARRAY_SIZE-1)/2)
    {
        ok = 0;
    }

    free(array);

    return ok;
}

int main(int argc, char* argv[])
{
    int ok = 1;
    int ok_tmp, iter;

    if (argc == 2)
    {
        iter = atoi(argv[1]);
    }
    else
    {
        printf("No number of iterations provided, using %d\n", DEFAULT_ITER);
    }

    for (int i = 0; i < iter; i++)
    {
        ok_tmp = foo();
        if (ok && !ok_tmp)
        {
            ok = 0;
        }
    }

    printf("%sOk\n", ok ? "": "Not ");

    return EXIT_SUCCESS;
}
