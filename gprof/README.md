# gprof

We need to compile with the `-pg` flag. Then run the program and analyze the
execution:
```bash
make
./main
gprof main gmon.out
```
The `-b` option of `gprof` allows to remove the verbosity and the
explanations in the output.
