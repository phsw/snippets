/* Implementation of Heap's algorithm to generate all permutations of a set.
 * See https://en.wikipedia.org/wiki/Heap%27s_algorithm
 */
#include <stdlib.h>
#include <stdio.h>

#define SIZE 4

#define SWAP(__x, __y) int __tmp = __x; __x = __y; __y = __tmp;

static inline void print_array(int* array)
{
    for (int i = 0; i < SIZE; i++)
    {
        printf("%d", array[i]);
        if (i < SIZE-1)
        {
            printf(", ");
        }
    }
    printf("\n");
}

int main(int argc, char* argv[])
{
    int array[SIZE];

    for (int i = 0; i < SIZE; i++)
    {
        array[i] = i;
    }

    /* Heap algorithm */
    int counter[SIZE] = {0};
    int i = 0;

    print_array(array);

    while (i < SIZE)
    {
        if (counter[i] < i)
        {
            if (i % 2 == 0)
            {
                SWAP(array[0], array[i]);
            }
            else
            {
                SWAP(array[counter[i]], array[i]);
            }

            print_array(array);

            counter[i]++;
            i = 0;
        }
        else
        {
            counter[i] = 0;
            i++;
        }
    }

    return EXIT_SUCCESS;
}
