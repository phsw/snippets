/* Inspired from:
 * - papi/src/examples/PAPI_add_remove_events.c
 */
#include <stdlib.h>
#include <stdio.h>
#include <papi.h>

volatile double a = 0.5, b = 2.2;

static void test_fail(const int line, const char* func, int retval)
{
    fprintf(stderr, "Error on line %d when calling %s: return value = %d\n", line, func, retval);
}

void dummy(void* array)
{
    /* Confuse the compiler so as not to optimize
       away the flops in the calling routine    */
    /* Cast the array as a void to eliminate unused argument warning */
    (void) array;
}

void do_flops(int n)
{
    int i;
    double c = 0.11;

    for (i = 0; i < n; i++ )
    {
        c += a * b;
    }

    dummy(( void*) &c);
}

int main(int argc, char* argv[])
{
    int nb_iter = 1000000;
    if (argc == 2)
    {
        nb_iter = atoi(argv[1]);
    }

    int event_set = PAPI_NULL;
    int retval;
    long long int values[PAPI_MAX_HWCTRS];

    retval = PAPI_library_init(PAPI_VER_CURRENT);
    if (retval != PAPI_VER_CURRENT)
    {
        test_fail(__LINE__, "PAPI_library_init", retval);
    }

    if ((retval = PAPI_query_event(PAPI_TOT_CYC)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_query_event", retval);
    }

    if ((retval = PAPI_query_event(PAPI_LD_INS)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_query_event", retval);
    }

    if ((retval = PAPI_create_eventset(&event_set)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_create_eventset", retval);
    }

    if ((retval = PAPI_add_event(event_set, PAPI_TOT_CYC)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_add_event", retval);
    }

    if ((retval = PAPI_add_event(event_set, PAPI_LD_INS)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_add_event", retval);
    }

    if ((retval = PAPI_start(event_set)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_start", retval);
    }

    do_flops(nb_iter);

    if ((retval = PAPI_stop(event_set, values)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_stop", retval);
    }

    printf("%d iterations\n", nb_iter);
    printf("PAPI_TOT_CYC: %lld\n", values[0]);
    printf("PAPI_LD_INS: %lld\n", values[1]);

    if ((retval = PAPI_remove_event(event_set, PAPI_TOT_CYC)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_remove_event", retval);
    }

    if ((retval = PAPI_remove_event(event_set, PAPI_LD_INS)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_remove_event", retval);
    }

    if ((retval = PAPI_destroy_eventset(&event_set)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_destroy_eventset", retval);
    }

    PAPI_shutdown();

    return EXIT_SUCCESS;
}
