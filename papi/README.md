PAPI library
============

Repository: https://bitbucket.org/icl/papi/src/master/

Documentation: https://bitbucket.org/icl/papi/wiki/Home

## Installation

Install packages `libpapi-dev`, `libpapi5.7` and `papi-tools`.

Set kernel parameter `kernel.perf_event_paranoid` to 2.

## Usage

**Warning**: do not use PAPI and likwid simultaneously ! It will make counter
values inconsistent. Also starting new *processes* leads to incoherent counter
values when thread inheritance is enabled.

Use `papi_avail` to discover available counters.

Use `papi_event_chooser PRESET <events separated by space>` to know if selected
events are compatible.


## Threads

By default, counters are related to the current thread. To use global counters,
you have to set the `inherit` option. See the `threads.c` example.
