/* Inspired from:
 * - papi/src/examples/PAPI_add_remove_events.c
 * - papi/src/ctests/zero_pthreads.c
 * - papi/src/ctests/inherit.c
 */
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <papi.h>

volatile double a = 0.5, b = 2.2;

static inline void test_fail(const int line, const char* func, int retval)
{
    fprintf(stderr, "Error on line %d when calling %s: return value = %d (%s)\n", line, func, retval, PAPI_strerror(retval));
}

void dummy(void* array)
{
    /* Confuse the compiler so as not to optimize
       away the flops in the calling routine    */
    /* Cast the array as a void to eliminate unused argument warning */
    (void) array;
}

void do_flops(int n)
{
    int i;
    double c = 0.11;

    for (i = 0; i < n; i++ )
    {
        c += a * b;
    }

    dummy(( void*) &c);
}

void* run_thread(void* ptr)
{
    do_flops(1000000);
    /*printf("End of thread flops\n");*/

    sleep(5);
    /*printf("End of thread sleep\n");*/

    do_flops(1000000);

    return NULL;
}

int main(int argc, char* argv[])
{
    int event_set = PAPI_NULL;
    PAPI_option_t papi_opt;
    int retval;
    int papi_event;
    char* papi_event_str = "PAPI_TOT_CYC";
    long long value;
    pthread_t pid;

    if ((retval = PAPI_library_init(PAPI_VER_CURRENT)) != PAPI_VER_CURRENT)
    {
        test_fail(__LINE__, "PAPI_library_init", retval);
    }

    if ((retval = PAPI_event_name_to_code(papi_event_str, &papi_event)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_event_name_to_code", retval);
    }

    if ((retval = PAPI_query_event(papi_event)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_query_event", retval);
    }

    if ((retval = PAPI_create_eventset(&event_set)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_create_eventset", retval);
    }

    /* Needed to support counter inheritance in threads */
    if ((retval = PAPI_assign_eventset_component(event_set, 0)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_assign_eventset_component", retval);
    }

    memset(&papi_opt, 0x0, sizeof(PAPI_option_t));
    papi_opt.inherit.inherit = PAPI_INHERIT_ALL;
    papi_opt.inherit.eventset = event_set;
    if ((retval = PAPI_set_opt(PAPI_INHERIT, &papi_opt)) != PAPI_OK )
    {
        test_fail(__LINE__, "PAPI_set_opt", retval);
    }
    /* End of needed code to support counter inheritance in threads */

    if ((retval = PAPI_add_event(event_set, papi_event)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_add_event", retval);
    }

    if ((retval = PAPI_start(event_set)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_start", retval);
    }

    do_flops(1000000);

    if ((retval = PAPI_stop(event_set, &value)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_stop", retval);
    }

    printf("From thread main: \t%s: %15lld\n", papi_event_str, value);

    if ((retval = PAPI_start(event_set)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_start", retval);
    }

    pthread_create(&pid, NULL, run_thread, NULL);

    sleep(2); // so the thread can do some flops

    if ((retval = PAPI_stop(event_set, &value)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_stop", retval);
    }

    printf("From other thread: \t%s: %15lld\n", papi_event_str, value);

    if ((retval = PAPI_start(event_set)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_start", retval);
    }

    do_flops(1000000);

    pthread_join(pid, NULL);

    if ((retval = PAPI_stop(event_set, &value)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_stop", retval);
    }

    printf("From both threads: \t%s: %15lld\n", papi_event_str, value);


    if ((retval = PAPI_start(event_set)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_start", retval);
    }

    #pragma omp parallel for
    for (int i = 10; i > 0; i--)
    {
        do_flops(1000000);
    }

    if ((retval = PAPI_stop(event_set, &value)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_stop", retval);
    }

    printf("From OpenMP workers: \t%s: %15lld\n", papi_event_str, value);

    if ((retval = PAPI_remove_event(event_set, papi_event)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_remove_event", retval);
    }

    if ((retval = PAPI_destroy_eventset(&event_set)) != PAPI_OK)
    {
        test_fail(__LINE__, "PAPI_destroy_eventset", retval);
    }

    PAPI_shutdown();

    return EXIT_SUCCESS;
}
