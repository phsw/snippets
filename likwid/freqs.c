#include <stdio.h>
#include <stdlib.h>
#include <likwid.h>

int main()
{
	topology_init();
	CpuInfo_t info = get_cpuInfo();
	CpuTopology_t topo = get_cpuTopology();

	int num_cores = topo->numHWThreads / topo->numThreadsPerCore;

	for (int c = 0; c < num_cores; c++)
	{
		thermal_init(c);
	}

	printf("%s with %d sockets and %d CPUs\n", info->name, topo->numSockets, topo->numHWThreads);
	printf("Hyperthreading is %s\n", (topo->numThreadsPerCore == 2) ? "enabled" : "disabled");
	printf("CPU frequencies between %lu and %lu MHz\n", freq_getCpuClockMin(0) / 1000, freq_getCpuClockMax(0) / 1000);
	printf("Uncore frequency between %lu and %lu MHz\n", freq_getUncoreFreqMin(0), freq_getUncoreFreqMax(0));

	printf("\n");

	for (int s = 0; s < topo->numSockets; s++)
	{
		printf("Uncore frequency of socket #%d: %lu MHz\n", s, freq_getUncoreFreqCur(s));
	}

	printf("\n");

	for (int c = 0; c < topo->numHWThreads; c++)
	{
		printf("Frequency of logical core #%d: %lu Hz\n", c, freq_getCpuClockCurrent(c));
	}

	printf("\n");

	uint32_t temp;
	for (int c = 0; c < num_cores; c++)
	{
		thermal_read(c, &temp);
		printf("Temperature of physical core #%d: %u °C\n", c, temp);
	}

	topology_finalize();
}
