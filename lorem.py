#!/usr/bin/env python3

# Adapted from Pypsum, GPL license Luca De Vitis <luca@monkeython.com> https://pypi.org/project/pypsum/

import urllib.request
import xml.dom.minidom as xml
from optparse import OptionParser

def get_lipsum(howmany, what, start_with_lipsum):
    query = {
        "amount": howmany,
        "what": what,
        "start": 'yes' if start_with_lipsum else 'no'
    }

    req = urllib.request.Request("http://www.lipsum.com/feed/xml/", urllib.parse.urlencode(query).encode("ascii"), headers={'User-Agent': 'Mozilla'})
    with urllib.request.urlopen(req) as f:
        s = f.read().decode()

    dom = xml.parseString(s)

    return dom.getElementsByTagName("lipsum")[0].firstChild.data, dom.getElementsByTagName("generated")[0].firstChild.data

get_lipsum.__doc__ = """Get lorem ipsum text from lipsum.com. Parameters:
howmany: how many items to get
what: the type of the items [paras/words/bytes/lists]
start_with_lipsum: whether or not you want the returned text to start with Lorem ipsum [True/False]
Returns a tuple with the generated text on the 0 index and generation statistics on index 1"""

if __name__ == "__main__":
    from optparse import OptionParser

    optionParser = OptionParser()
    optionParser.add_option(
        "-n","--howmany",
        type="int",
        dest="howmany",
        metavar="X",
        help="how many items to get"
    )
    whatChoices = ('paras','words','bytes','lists')
    optionParser.add_option(
        "-w","--what",
        choices=whatChoices,
        dest="what",
        metavar="TYPE",
        help="the type of items to get: " + ', '.join(whatChoices)
    )
    optionParser.add_option(
        "-l","--start-with-Lorem",
        action="store_true",
        dest="lipsum",
        help='Start the text with "Lorem ipsum"'
    )
    optionParser.set_defaults(
        lipsum=False,
        howmany=5,
        what="paras"
    )
    opts, _ = optionParser.parse_args()

    lipsum = get_lipsum(opts.howmany, opts.what, opts.lipsum)

    print(lipsum[0] + "\n\n" + lipsum[1])
