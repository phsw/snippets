#include <stdio.h>
#include <sched.h>
#include <unistd.h>
#include <omp.h>

int main()
{
	printf("The keyword 'for' is mandatory to parallelize for loops:\n");
	printf("Without:\n");
	#pragma omp parallel
	for (int i = 0; i < 5; i++)
	{
		int thread_num = omp_get_thread_num();
		int s = 0;
		for (int j = 0; j < 1000000; j++)
		{
			s += 1;
		}
		printf("Thread %3d sum = %3d\n", thread_num, s);
	}

	printf("With:\n");
	#pragma omp parallel for
	for (int i = 0; i < 5; i++)
	{
		int thread_num = omp_get_thread_num();
		int s = 0;
		for (int j = 0; j < 1000000; j++)
		{
			s += 1;
		}
		printf("Thread %3d sum = %3d\n", thread_num, s);
	}

	printf("\n");


	printf("If a variable is declared before the parallel region, it has to be protected:\n");
	int j = 0;
	printf("Without private:\n");
	#pragma omp parallel for
	for (int i = 0; i < 5; i++)
	{
		int thread_num = omp_get_thread_num();
		int s = 0;
		for (j = 0; j < 1000000; j++)
		{
			s += 1;
		}
		printf("Thread %3d sum = %3d\n", thread_num, s);
	}
	printf("With private:\n");
	#pragma omp parallel for private(j)
	for (int i = 0; i < 5; i++)
	{
		int thread_num = omp_get_thread_num();
		int s = 0;
		for (j = 0; j < 1000000; j++)
		{
			s += 1;
		}
		printf("Thread %3d sum = %3d\n", thread_num, s);
	}



	return 0;
}
