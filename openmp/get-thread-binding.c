/* Run with
 * OMP_NUM_THREADS=4 OMP_PROC_BIND=true OMP_PLACES=cores hwloc-bind --cpubind core:0-4 ./get-thread-binding
 *
 * We can have information about the binding with OMP_DISPLAY_AFFINITY=true.
 *
 * From https://stackoverflow.com/a/40158426
 */
#define _GNU_SOURCE // sched_getcpu(3) is glibc-specific (see the man page)

#include <stdio.h>
#include <sched.h>
#include <unistd.h>
#include <omp.h>

int main()
{
	#pragma omp parallel
	{
		int thread_num = omp_get_thread_num();
		int cpu_num = sched_getcpu();
		printf("Thread %3d is running on CPU %3d\n", thread_num, cpu_num);
		sleep(5);
	}

	return 0;
}
