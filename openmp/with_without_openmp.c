#include <stdio.h>
#ifdef _OPENMP
#include <omp.h>
#endif

int main()
{
#if defined(_OPENMP)
	printf("With OpenMP (_OPENMP = %d)\n", _OPENMP);
	#pragma omp parallel
	{
		int thread_num = omp_get_thread_num();
		printf("Thread %3d is running from OpenMP\n", thread_num);
	}
#else
	printf("Without OpenMP\n");
#endif

	return 0;
}
