/* Run with
 * OMP_NUM_THREADS=4 OMP_PROC_BIND=true OMP_PLACES=cores hwloc-bind --cpubind core:0-4 ./get-thread-binding
 *
 * We can have information about the binding with OMP_DISPLAY_AFFINITY=true.
 *
 * From https://stackoverflow.com/a/40158426
 *
 * The difference with get-thread-binding.c is that this bench returns the core
 * logical index instead of the PU (hyperthread) index.
 */
#define _GNU_SOURCE // sched_getcpu(3) is glibc-specific (see the man page)

#include <stdio.h>
#include <sched.h>
#include <unistd.h>
#include <omp.h>
#include <hwloc.h>

int main()
{
	hwloc_topology_t topology;

	hwloc_topology_init(&topology);
	hwloc_topology_load(topology);

	#pragma omp parallel
	{
		int thread_num = omp_get_thread_num();
		int getcpu_num = sched_getcpu(); // returns the physical index of the PU

		hwloc_obj_t pu_obj = hwloc_get_pu_obj_by_os_index(topology, getcpu_num);
		assert(pu_obj->type == HWLOC_OBJ_PU);

		hwloc_obj_t core_obj = pu_obj->parent;
		assert(core_obj->type == HWLOC_OBJ_CORE);


		printf("Thread %d is running on PU P#%d (L#%d) which is on Core L#%d\n", thread_num, getcpu_num, pu_obj->logical_index, core_obj->logical_index);
		sleep(5);
	}

	hwloc_topology_destroy(topology);

	return 0;
}
