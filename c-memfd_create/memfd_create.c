#define _GNU_SOURCE // for memfd_create

/* See also:
 * - https://x-c3ll.github.io/posts/2018/02/02/fileless-memfd_create.html
 * - https://github.com/a-darwish/memfd-examples
 *
 * Instead of memfd_create, we could also use :
 * - open(O_TMPFILE)
 * - shm_open()
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>


int main(int argc, char* argv[])
{
    int fd = memfd_create("test", 0);
    if (fd < 0)
    {
        perror("memfd_create");
        return EXIT_FAILURE;
    }

    int ret = ftruncate(fd, 1024);
    if (ret != 0)
    {
        perror("ftruncate");
        return EXIT_FAILURE;
    }

    FILE* file_fd = fdopen(fd, "w+");
    if (file_fd == NULL)
    {
        perror("fdopen");
        return EXIT_FAILURE;
    }

    char* to_write= "foo\n";

    printf("Writing '%s'...\n", to_write);

    ret = fprintf(file_fd, to_write);
    if (ret < 0)
    {
        perror("fprintf");

        fclose(file_fd);
        close(fd);

        return EXIT_FAILURE;
    }

    rewind(file_fd);

    char line[256];
    fgets(line, 256, file_fd);
    printf("Read '%s'\n", line);

    fclose(file_fd);
    close(fd);

    return EXIT_SUCCESS;
}
