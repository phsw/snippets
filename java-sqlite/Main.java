/*
 * Some examples:
 * https://www.tutorialspoint.com/sqlite/sqlite_java.htm
 * https://zestedesavoir.com/tutoriels/646/apprenez-a-programmer-en-java/559_interactions-avec-les-bases-de-donnees/2722_fouiller-dans-sa-base-de-donnees/
 * https://www.tutlane.com/tutorial/sqlite/sqlite-java-tutorial
 * https://jean-luc-massat.pedaweb.univ-amu.fr/ens/jee/jdbc.html
 * https://guendouz.wordpress.com/2012/11/23/utilisation-de-la-base-de-donnees-sqlite-en-java/
 *
 * Download the SQLite JDBC driver here: https://github.com/xerial/sqlite-jdbc/releases/
 */
import java.io.File;
import java.sql.*;

public class Main
{
    static public void createTable(Connection c) throws SQLException
    {
        Statement stmt = c.createStatement();
        String sql = "CREATE TABLE characters " +
            "(id            INT PRIMARY KEY NOT NULL," +
            " name          CHAR(100) NOT NULL, " +
            " family_name   CHAR(100) NOT NULL, " +
            " gender        CHAR(10)  NOT NULL, " +
            " job           CHAR(100) NOT NULL)";
        stmt.execute(sql);
        stmt.close();
    }

    static public void addCharacter(Connection c, int id, String name, String familyName, String gender, String job) throws SQLException
    {
        PreparedStatement stmt = c.prepareStatement("INSERT INTO characters VALUES (?, ?, ?, ?, ?);");
        stmt.setInt(1, id);
        stmt.setString(2, name);
        stmt.setString(3, familyName);
        stmt.setString(4, gender);
        stmt.setString(5, job);
        stmt.execute();
        stmt.close();
    }

    static public void removeCharacter(Connection c, int id) throws SQLException
    {
        PreparedStatement stmt = c.prepareStatement("DELETE FROM characters WHERE id = ?;");
        stmt.setInt(1, id);
        stmt.execute();
        stmt.close();
    }

    static public void populateTable(Connection c) throws SQLException
    {
        addCharacter(c, 1, "Sheldon", "Cooper", "Male", "Researcher");
    }

    static public void printCharacter(ResultSet rs) throws SQLException
    {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String family_name = rs.getString("family_name");
        String gender = rs.getString("gender");
        String job = rs.getString("job");

        System.out.println(name + " " + family_name + " (#" + id + ") is a " + gender + " and is a " + job + ".");
    }

    static public void listCharacters(Connection c) throws SQLException
    {
        Statement stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery( "SELECT * FROM characters;" );

        System.out.println("List of characters:");

        while (rs.next())
        {
            printCharacter(rs);
        }

        rs.close();
        stmt.close();
    }

    static public void main(String[] args)
    {
        String dbFilename = "test.db";
        Connection c = null;
        File dbFile = new File(dbFilename);
        Statement stmt = null;
        boolean dbAlreadyExists = dbFile.exists();

        try
        {
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFilename);
            System.out.println("Opened database successfully");

            if (!dbAlreadyExists)
            {
                createTable(c);
                populateTable(c);
            }

            listCharacters(c);

            addCharacter(c, 2, "Howard", "Wolowitz", "Male", "Engineer");

            listCharacters(c);

            removeCharacter(c, 2);

            listCharacters(c);

            c.close();
        }
        catch (Exception e)
        {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

    }
}
