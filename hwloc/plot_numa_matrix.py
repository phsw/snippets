import argparse

from matplotlib import pyplot as plt
import pandas as pd

cli_parser = argparse.ArgumentParser()
cli_parser.add_argument("input_file")

args = cli_parser.parse_args()

df = pd.read_csv(args.input_file, delimiter="\t")
df.set_index("Src Node", inplace=True)

plt.imshow(df[::-1])

colorbar = plt.colorbar()
if "bandwidth" in args.input_file:
    colorbar.set_label("Bandwidth (MB/s)")
elif "latency" in args.input_file:
    colorbar.set_label("Latency (µs)")

plt.xticks(range(len(df)), df.columns)
plt.yticks(range(len(df)), df.index[::-1])
plt.title(args.input_file)

plt.savefig(args.input_file + ".png")

plt.show()
