/**
 * Compute the performances of memory access accross different NUMA nodes
 * (latency and bandwidth).
 *
 * Highlhy inspired from StarPU/src/core/perfmodel/perfmodel_bus.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <hwloc.h>
#include <string.h>
#include <time.h>

#if HWLOC_API_VERSION < 0x00010b00
#define HWLOC_OBJ_NUMANODE HWLOC_OBJ_NODE
#endif

#define MAX_NUMA_NODES 8
#define SIZE (512*1024*1024*sizeof(char))
#define NITER 32

enum matrix_type_e {
	LATENCY,
	BANDWIDTH,
};

enum bench_type_e {
	MEMSET,
	MEMCPY,
};

static double numa_latency[MAX_NUMA_NODES][MAX_NUMA_NODES];
static double numa_bandwidth[MAX_NUMA_NODES][MAX_NUMA_NODES];
static int nb_numa_nodes;
static hwloc_topology_t topology;

static inline double timing_now_us()
{
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);

	return (1000000.0*now.tv_sec) + (0.001*now.tv_nsec);
}

static void measure_bandwidth_latency_between_numa(enum bench_type_e bench_type, int numa_src, int numa_dst)
{
	double start, end, timing;
	unsigned iter;

	int depth = hwloc_get_type_or_below_depth(topology, HWLOC_OBJ_CORE);

	hwloc_obj_t obj_src = hwloc_get_obj_by_type(topology, HWLOC_OBJ_NUMANODE, numa_src);


	/* Bind the thread doing the memcpy on the src NUMA node, to avoid
	 * spurious copies: */
	hwloc_cpuset_t cpuset = hwloc_bitmap_dup(obj_src->cpuset);
	hwloc_bitmap_singlify(cpuset);
	if (hwloc_set_cpubind(topology, cpuset, 0))
	{
		char *str;
		int error = errno;
		hwloc_bitmap_asprintf(&str, obj_src->cpuset);
		printf("Couldn't bind to cpuset %s: %s\n", str, strerror(error));
		free(str);
	}

	hwloc_bitmap_free(cpuset);

	unsigned char *h_buffer;
#if HWLOC_API_VERSION >= 0x00020000
	h_buffer = hwloc_alloc_membind(topology, SIZE, obj_src->nodeset, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_BYNODESET | HWLOC_MEMBIND_NOCPUBIND);
#else
	h_buffer = hwloc_alloc_membind_nodeset(topology, SIZE, obj_src->nodeset, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_NOCPUBIND);
#endif

	unsigned char *d_buffer;
	hwloc_obj_t obj_dst = hwloc_get_obj_by_type(topology, HWLOC_OBJ_NUMANODE, numa_dst);
#if HWLOC_API_VERSION >= 0x00020000
	d_buffer = hwloc_alloc_membind(topology, SIZE, obj_dst->nodeset, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_BYNODESET | HWLOC_MEMBIND_NOCPUBIND);
#else
	d_buffer = hwloc_alloc_membind_nodeset(topology, SIZE, obj_dst->nodeset, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_NOCPUBIND);
#endif

	memset(h_buffer, 'a', SIZE);
	memset(d_buffer, 'b', SIZE);

	start = timing_now_us();
	if (bench_type == MEMCPY)
	{
		for (iter = 0; iter < NITER; iter++)
		{
			memcpy(d_buffer, h_buffer, SIZE);
		}
	}
	else
	{
		for (iter = 0; iter < NITER; iter++)
		{
			memset(d_buffer, 'c', SIZE);
		}
	}
	end = timing_now_us();
	timing = end - start;

	numa_bandwidth[numa_src][numa_dst] = ((double) SIZE)*((double) NITER)/timing;

	start = timing_now_us();
	if (bench_type == MEMCPY)
	{
		for (iter = 0; iter < NITER; iter++)
		{
			memcpy(d_buffer, h_buffer, 1);
		}
	}
	else
	{
		for (iter = 0; iter < NITER; iter++)
		{
			memset(d_buffer, 'c', 1);
		}
	}
	end = timing_now_us();
	timing = end - start;

	numa_latency[numa_src][numa_dst] = timing/NITER;

	hwloc_free(topology, h_buffer, SIZE);
	hwloc_free(topology, d_buffer, SIZE);
}

void write_matrix(FILE* f, enum matrix_type_e matrix_type)
{
	int src, dst;
	double value;

	fprintf(f, "Src Node");
	for (dst = 0; dst < nb_numa_nodes; dst++)
	{
		fprintf(f, "\tto %d", dst);
	}
	fprintf(f, "\n");

	for (src = 0; src < nb_numa_nodes; src++)
	{
		fprintf(f, "from %d\t", src);
		for (dst = 0; dst < nb_numa_nodes; dst++)
		{
			/*if (src == dst)
			{
				value = 0.0;
			}
			else*/ if (matrix_type == LATENCY)
			{
				value = numa_latency[src][dst];
			}
			else if (matrix_type == BANDWIDTH)
			{
				value = numa_bandwidth[src][dst];
			}

			if (dst > 0)
			{
				fprintf(f, "\t");
			}
			fprintf(f, "%e", value);
		}
		fprintf(f, "\n");
	}
}

int main(int argc, char* argv[])
{
	int i, j;
	enum bench_type_e bench_type = MEMCPY;

	if (argc == 2 && !strcasecmp(argv[1], "memset"))
	{
		bench_type = MEMSET;
	}

	printf("Will do %s\n", bench_type == MEMCPY ? "memcpy" : "memset");

	hwloc_topology_init(&topology);
	hwloc_topology_load(topology);

	nb_numa_nodes = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_NUMANODE);
	printf("Found %d NUMA node%s\n", nb_numa_nodes, nb_numa_nodes > 1 ? "s" : "");

	for (i = 0; i < nb_numa_nodes; i++)
	{
		for (j = 0; j < nb_numa_nodes; j++)
		{
			// if (i != j)
			{
				printf("NUMA %d -> %d...\n", i, j);
				measure_bandwidth_latency_between_numa(bench_type, i, j);
			}
		}
	}

	printf("Latency (µs, with 1 B data chunks):\n");
	write_matrix(stdout, LATENCY);
	printf("Bandwidth (MB/s, with %d MB data chunks):\n", SIZE/(1024*1024));
	write_matrix(stdout, BANDWIDTH);

	hwloc_topology_destroy(topology);

	return EXIT_SUCCESS;
}
