#include <stdio.h>
#include <stdlib.h>
#include <hwloc.h>

int main(int argc, char* argv[])
{
        hwloc_topology_t topology;

        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        hwloc_obj_t core = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, 0); /* Core. A computation unit (may be shared by several PUs, aka logical processors). */

        hwloc_obj_t parent = core->parent;

        while (parent->arity == 1)
        {
                parent = parent->parent;
        }

        printf("Number of cores: %u\n", parent->arity);

        hwloc_topology_destroy(topology);

        return EXIT_SUCCESS;
}
