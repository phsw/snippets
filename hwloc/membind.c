#include <stdio.h>
#include <stdlib.h>
#include <hwloc.h>

#if HWLOC_API_VERSION < 0x00010b00
#define HWLOC_OBJ_NUMANODE HWLOC_OBJ_NODE
#endif

int main(int argc, char* argv[])
{
        hwloc_topology_t topology;

        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        int nb_numa = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_NUMANODE);
        printf("%d NUMA node%s\n", nb_numa, nb_numa > 1 ? "s" : "");

        int** memories = malloc(nb_numa*sizeof(int*));

        for (int i = 0; i < nb_numa; i++)
        {
                hwloc_obj_t numa_node = hwloc_get_obj_by_type(topology, HWLOC_OBJ_NUMANODE, i);
                /* HWLOC_MEMBIND_BIND: Allocate memory on the specified nodes.
                 * HWLOC_MEMBIND_BYNODESET: Consider the bitmap argument as a nodeset.
                 * HWLOC_MEMBIND_NOCPUBIND: Avoid any effect on CPU binding. */
#if HWLOC_API_VERSION >= 0x00020000
                memories[i] = hwloc_alloc_membind(topology, sizeof(int), numa_node->nodeset, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_BYNODESET | HWLOC_MEMBIND_NOCPUBIND);
#else
                memories[i] = hwloc_alloc_membind_nodeset(topology, sizeof(int), numa_node->nodeset, HWLOC_MEMBIND_BIND, HWLOC_MEMBIND_NOCPUBIND);

#endif
                *memories[i] = i;
        }

        int* a = malloc(sizeof(int));
        *a = nb_numa;

        for (int i = 0; i < nb_numa; i++)
        {
                printf("NUMA node %d: %p = %d (should be %d)\n", i, memories[i], *memories[i], i);
        }
        printf("Main memory: %p = %d (should be %d)\n", a, *a, nb_numa);

        for (int i = 0; i < nb_numa; i++)
        {
                hwloc_free(topology, memories[i], sizeof(int));
        }
        free(memories);
        free(a);

        hwloc_topology_destroy(topology);

        return EXIT_SUCCESS;
}
