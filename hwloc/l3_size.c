#include <stdio.h>
#include <stdlib.h>
#include <hwloc.h>
/*#include <hwloc/helper.h>*/

int main(int argc, char* argv[])
{
        hwloc_topology_t topology;

        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

#if HWLOC_API_VERSION >= 0x00020000
        hwloc_obj_t l3_obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_L3CACHE, 0);
#else
        int cache_depth = hwloc_get_cache_type_depth(topology, 3, HWLOC_OBJ_CACHE_UNIFIED);
        assert(cache_depth > 0);
        hwloc_obj_t l3_obj = hwloc_get_obj_by_depth(topology, cache_depth, 0);
#endif
        printf("Size of L3 cache: %lu B (%lu MB)\n", l3_obj->attr->cache.size, l3_obj->attr->cache.size / (1024*1024));

        hwloc_topology_destroy(topology);

        return EXIT_SUCCESS;
}
