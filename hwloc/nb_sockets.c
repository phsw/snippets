#include <stdio.h>
#include <stdlib.h>
#include <hwloc.h>

int main(int argc, char* argv[])
{
        hwloc_topology_t topology;

        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        printf("Number of packages (sockets): %d\n", hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_PACKAGE));

        hwloc_topology_destroy(topology);

        return EXIT_SUCCESS;
}
