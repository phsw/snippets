#include <stdio.h>
#include <stdlib.h>
#include <hwloc.h>

int main(int argc, char* argv[])
{
        hwloc_topology_t topology;
        hwloc_bitmap_t cpuset = hwloc_bitmap_alloc();

        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        int ret = hwloc_get_cpubind(topology, cpuset, HWLOC_CPUBIND_THREAD);
        if (ret)
        {
                fprintf(stderr, "Error: could not get current CPU binding: %s\n", strerror(errno));
                goto end;
        }

        char*  str;
        hwloc_bitmap_asprintf(&str, cpuset);

        hwloc_obj_t largest_obj = hwloc_get_first_largest_obj_inside_cpuset(topology, cpuset);

        printf("Bound to %s (%s)\n", str, hwloc_obj_type_string(largest_obj->type));
        free(str);

        int n_cores = hwloc_get_nbobjs_inside_cpuset_by_type(topology, cpuset, HWLOC_OBJ_CORE);
        if (n_cores > 1)
        {
                printf("Warning, the process is bound on %d cores\n", n_cores);
        }
        else
        {
                char* object_type_str;
                hwloc_obj_type_t obj_type;

                if (n_cores == 0)
                {
                        /* No core: bound to a hyperthread */
                        object_type_str = "PU";
                        obj_type = HWLOC_OBJ_PU;
                }
                else
                {
                        object_type_str = "Core";
                        obj_type = HWLOC_OBJ_CORE;
                }
                struct hwloc_location loc;
                hwloc_obj_t numa_nodes[2];
                unsigned n_numa_parent_core = 2;
                loc.type = HWLOC_LOCATION_TYPE_OBJECT;

                hwloc_obj_t obj = hwloc_get_obj_inside_cpuset_by_type(topology, cpuset, obj_type, 0);
                if (obj == NULL)
                {
                        fprintf(stderr, "Error: no PU or core was found\n");
                        goto end;
                }

                loc.location.object = obj;
                hwloc_get_local_numanode_objs(topology, &loc, &n_numa_parent_core, numa_nodes, HWLOC_LOCAL_NUMANODE_FLAG_LARGER_LOCALITY);
                if (n_numa_parent_core > 1)
                {
                        printf("Warning: the core is in several NUMA nodes\n");
                }

                hwloc_obj_t package_obj = hwloc_get_ancestor_obj_by_type(topology, HWLOC_OBJ_PACKAGE, obj);
                printf("Bound on %s #%d belonging to NUMA node #%d belonging to package #%d\n", object_type_str, obj->logical_index, numa_nodes[0]->logical_index, package_obj->logical_index);
        }

end:
        hwloc_topology_destroy(topology);
        hwloc_bitmap_free(cpuset);

        return EXIT_SUCCESS;
}
