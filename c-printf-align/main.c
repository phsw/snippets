/* From https://stackoverflow.com/a/14420963 */
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	printf("Align left:\n");
	printf("%-12s%-12d%-12d\n", "a", 2989, 9283019);
	printf("%-12s%-12d%-12d\n", "helloworld", 0, 1828274198);

	printf("Align right:\n");
	printf("%-12s% 12d% 12d\n", "a", 2989, 9283019);
	printf("%-12s% 12d% 12d\n", "helloworld", 0, 1828274198);
	exit(0);
}
