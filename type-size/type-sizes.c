#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    printf("sizeof double: %ld\n", sizeof(double));
    printf("sizeof float: %ld\n", sizeof(float));
    printf("sizeof uint64_t: %ld\n", sizeof(unsigned long));
    printf("sizeof int: %ld\n", sizeof(int));
    printf("sizeof long double: %ld\n", sizeof(long double));
    printf("sizeof 4800: %ld\n", sizeof(4800));

    return EXIT_SUCCESS;
}
