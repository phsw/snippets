#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>

int main()
{
    int ret;
    int is_running = 1;

    // SDL initialisation with video support
    ret = SDL_Init(SDL_INIT_VIDEO);
    if (ret != 0)
    {
        fprintf(stderr, "Could not init SDL: %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    // Create the window
    SDL_Window *screen = SDL_CreateWindow(
        "SDL app",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        200, 200,
        0
    );
    if (!screen)
    {
        fprintf(stderr, "Could not create SDL screen: %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    // Create the renderer, can be seen as a paint brush
    SDL_Renderer *renderer = SDL_CreateRenderer(screen, -1, SDL_RENDERER_SOFTWARE);
    if (!renderer)
    {
        fprintf(stderr, "Could not create SDL renderer: %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    while (is_running)
    {
        SDL_Event event;
        SDL_WaitEvent(&event);

        switch (event.type)
        {
            case SDL_QUIT:
                is_running = 0;
                break;

            case SDL_KEYDOWN:
                // A keyboard key was pressed down
                switch (event.key.keysym.sym)
                {
                    // It was a `q`, quit the program by exiting this loop
                    case SDLK_q:
                        is_running = 0;
                        printf("Q was pressed\n");
                        break;
                }
                break;
        }
    }

    // Free all created resources
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(screen);

    // Quit the SDL program
    SDL_Quit();
    printf("ok\n");

    return EXIT_SUCCESS;
}
