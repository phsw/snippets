#!/bin/bash

# Script to test the application in a headless system
# Requires packages xvfb and xdotool

export DISPLAY=:44
Xvfb $DISPLAY &
pid_xvfb=$!

#valgrind ./main &
./main &
pid_main=$!

sleep 7

xdotool key q

sleep 3
kill $pid_xvfb
