# https://zetcode.com/python/jinja/

from jinja2 import Template, Environment


def own_filter(value, param):
    return value.replace("o", param)

jinja_env = Environment()
jinja_env.filters["own_filter"] = own_filter

tm = jinja_env.from_string("{{ variable|own_filter('a') }}")
print(tm.render(variable="foo"))
