#define _GNU_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#define MMAP_FILENAME "mmap.raw"

int main()
{
    long page_size = sysconf(_SC_PAGE_SIZE);

    int mmap_fd = open(MMAP_FILENAME, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
    if (mmap_fd < 0)
    {
      fprintf(stderr, "Could not open %s: %s\n", MMAP_FILENAME, strerror(errno));
      return EXIT_FAILURE;
    }

    if (ftruncate(mmap_fd, page_size) != 0)
    {
      fprintf(stderr, "Could not truncate %s: %s\n", MMAP_FILENAME, strerror(errno));
      return EXIT_FAILURE;
    }

    void* mmap_area = mmap(NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, mmap_fd, 0);
    if (mmap_area == MAP_FAILED)
    {
      fprintf(stderr, "Could not mmap: %s\n", strerror(errno));
      return EXIT_FAILURE;
    }

    ((char*) mmap_area)[0] = 'a';

    if (ftruncate(mmap_fd, 2*page_size) != 0)
    {
      fprintf(stderr, "Could not truncate %s: %s\n", MMAP_FILENAME, strerror(errno));
      return EXIT_FAILURE;
    }

    mmap_area = mremap(mmap_area, page_size, 2*page_size, MREMAP_MAYMOVE);
    if (mmap_area == MAP_FAILED)
    {
      fprintf(stderr, "Could not mremap: %s\n", strerror(errno));
      return EXIT_FAILURE;
    }

    ((char*) mmap_area)[4098] = 'b';

    munmap(mmap_area, 2*page_size);
    close(mmap_fd);


    // Do it again, with a PRIVATE mmap, only to read data:

    mmap_fd = open(MMAP_FILENAME, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
    if (mmap_fd < 0)
    {
      fprintf(stderr, "Could not open %s: %s\n", MMAP_FILENAME, strerror(errno));
      return EXIT_FAILURE;
    }

    mmap_area = mmap(NULL, 2*page_size, PROT_READ|PROT_WRITE, MAP_PRIVATE, mmap_fd, 0);
    if (mmap_area == MAP_FAILED)
    {
      fprintf(stderr, "Could not mmap: %s\n", strerror(errno));
      return EXIT_FAILURE;
    }

    printf("%c = a; %c = b\n", ((char*) mmap_area)[0], ((char*) mmap_area)[4098]);

    munmap(mmap_area, 2*page_size);
    close(mmap_fd);

    return EXIT_SUCCESS;
}
