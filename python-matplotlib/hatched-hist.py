# Makes a stacked histogram with several data series, one color per series and
# had a hatch to better distinguish colors.
#
# Adapted from https://matplotlib.org/3.4.2/gallery/lines_bars_and_markers/filled_step.html

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors


def stack_hist(ax, stacked_data, nbins, labels):
    min_value = stacked_data[0][0]
    max_value = min_value

    for series in stacked_data:
        for v in series:
            if v < min_value:
                min_value = v
            elif v > max_value:
                max_value = v

    bins = np.linspace(min_value, max_value, nbins, endpoint=True)
    data_colors = [colors.hsv_to_rgb([(i * 0.618033988749895) % 1.0, 0.7, 1]) for i in range(len(stacked_data))]
    hatches = ['/', '\\', '|', '-', '+', 'x', 'o', 'O', '.', '*']
    bottoms = np.zeros(nbins-1)

    for j, (data, label) in enumerate(zip(stacked_data, labels)):
        vals, edges = np.histogram(data, bins=bins)
        top = bottoms + vals
        ax.fill_between(edges, np.append(top, top[-1]), np.append(bottoms, bottoms[-1]), step='post', label=label, facecolor=data_colors[j], hatch=hatches[j % len(hatches)])
        bottoms = top



# Fixing random state for reproducibility
np.random.seed(19680801)

stack_data = np.random.randn(4, 12250)


fig, ax = plt.subplots()
stack_hist(ax, stack_data, 20, ["a", "b", "c", "d"])

ax.set_ylabel('counts')
ax.set_xlabel('x')
ax.legend()
plt.show()
