from matplotlib import pyplot as plt


NB = 4

fig, axs = plt.subplots(NB, NB, sharex=True, sharey=True, gridspec_kw = {'wspace':0.05, 'hspace':0.15})

handles_left = None
labels_left = None
handles_right = None
labels_right = None

for i in range(NB):
    for j in range(NB):
        axs[i, j].plot(range(10), range(10), label="Curve")
        axs[i, j].set_xlabel("X label")
        axs[i, j].set_ylabel("Y label")
        axs[i, j].set(title=f"Title {i}x{j}")
        axs[i, j].grid()
        axs[i, j].label_outer()  # hide labels on non-left and non-bottom plots, has to be called after setting labels

        handles_left, labels_left = axs[i, j].get_legend_handles_labels()

        ax_right = axs[i, j].twinx()
        ax_right.plot(range(10), list(map(lambda x: x*2.7, list(range(0, 10))[::-1])), label="Curve right")
        if j == (NB-1):
            ax_right.set_ylabel("Y label right")
        else:
            ax_right.yaxis.set_tick_params(which="both", labelright=False)
        handles_right, labels_right = ax_right.get_legend_handles_labels()


fig.legend(handles_left, labels_left, loc='lower left')
fig.legend(handles_right, labels_right, loc='lower right')

default_size = plt.rcParams["figure.figsize"]
fig.set_size_inches(default_size[0] * NB, default_size[1] * NB)

plt.savefig("test.png", bbox_inches="tight")
plt.show()
