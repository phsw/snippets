#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <hwloc.h>
#include <hwloc/helper.h>

static hwloc_topology_t topology;
static int topo_pu_depth;
static int nb_pus;

static void* thread_func(void* arg)
{
	// Bind to last core:
	hwloc_obj_t pu = hwloc_get_obj_by_depth(topology, topo_pu_depth, nb_pus-1);
	hwloc_cpuset_t cpuset = hwloc_bitmap_dup(pu->cpuset);
	hwloc_bitmap_singlify(cpuset);
	hwloc_set_cpubind(topology, cpuset, HWLOC_CPUBIND_THREAD);
	hwloc_bitmap_free(cpuset);

	printf("The thread is bound. Now sleeping to let you time to check the binding.\n");
	sleep(10);

	return NULL;
}

int main(int argc, char* argv[])
{
	pthread_t thread;

	hwloc_topology_init(&topology);
	hwloc_topology_load(topology);

	topo_pu_depth = hwloc_get_type_depth(topology, HWLOC_OBJ_CORE);
	assert(topo_pu_depth != HWLOC_TYPE_DEPTH_UNKNOWN);

	nb_pus = hwloc_get_nbobjs_by_depth(topology, topo_pu_depth);

	pthread_create(&thread, NULL, thread_func, NULL);

	pthread_join(thread, NULL);

	hwloc_topology_destroy(topology);

	return EXIT_SUCCESS;
}
