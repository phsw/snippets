#ifndef __TIMING_H__
#define __TIMING_H__

#include <time.h>

static inline struct timespec timespec_diff(const struct timespec*const t1, const struct timespec*const t2)
{
  struct timespec diff;
  if (t2->tv_nsec > t1->tv_nsec)
  {
    diff.tv_sec  = t2->tv_sec - t1->tv_sec;
    diff.tv_nsec = t2->tv_nsec - t1->tv_nsec;
  }
  else
  {
    diff.tv_sec  = t2->tv_sec - t1->tv_sec - 1;
    diff.tv_nsec = t2->tv_nsec - t1->tv_nsec + 1000000000L;
  }
  return diff;
}

static inline double timespec2usec(const struct timespec t)
{
  return 1000000.0 * t.tv_sec + t.tv_nsec / 1000.0;
}

static inline void precise_sleep(float nb_sec)
{
  /* This version will always sleep for at least ~ 50 µs, and we need to sleep
   * for 0.2 µs, so don't use it */
  struct timespec req, rem;

  req.tv_sec = nb_sec;
  req.tv_nsec = (nb_sec - (float) req.tv_sec) * 1000000000;
  while (nanosleep(&req, &rem))
    req = rem;
}

static inline void very_precise_sleep(float nb_sec)
{
  /* This version will always sleep for at least 0.08 µs */
  struct timespec timespec_begin, timespec_end;
  clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_begin);
  do {
    clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_end);
  } while(timespec2usec(timespec_diff(&timespec_begin, &timespec_end)) < 1e6*nb_sec);
}
#endif
