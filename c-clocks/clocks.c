// https://stackoverflow.com/questions/4986818/how-to-sleep-for-a-few-microseconds

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "timing.h"

#define NB_ITER 10000


static inline uint64_t rdtsc_get_tick(void)
{
#ifdef __i386
	uint64_t r;
	__asm__ volatile ("rdtsc" : "=A" (r));
	return r;
#elif defined __amd64
	uint64_t a, d;
	__asm__ volatile ("rdtsc" : "=a" (a), "=d" (d));
	return (d<<32) | a;
#else
#error
#endif
}

static inline void overhead_precise_sleep(float total_duration)
{
	struct timespec timespec_begin, timespec_end;
	float sleep_duration = total_duration / NB_ITER;

	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_begin);
	for (int i = 0; i < NB_ITER; i++)
	{
		precise_sleep(sleep_duration);
	}
	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_end);

	printf(
		"When required to sleep %.4lf us, precise_sleep actually sleeps %.4lf us\n",
		1e6 * sleep_duration,
		timespec2usec(timespec_diff(&timespec_begin, &timespec_end)) / (NB_ITER+1)
	);
}

static inline void overhead_very_precise_sleep(float total_duration)
{
	struct timespec timespec_begin, timespec_end;
	float sleep_duration = total_duration / NB_ITER;

	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_begin);
	for (int i = 0; i < NB_ITER; i++)
	{
		very_precise_sleep(sleep_duration);
	}
	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_end);

	printf(
		"When required to sleep %.4lf us, very_precise_sleep actually sleeps %.4lf us\n",
		1e6 * sleep_duration,
		timespec2usec(timespec_diff(&timespec_begin, &timespec_end)) / (NB_ITER+1)
	);
}


static inline void overhead_usleep(float total_duration)
{
	struct timespec timespec_begin, timespec_end;
	unsigned int sleep_duration = 1e6 * total_duration / NB_ITER;

	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_begin);
	for (int i = 0; i < NB_ITER; i++)
	{
		usleep(sleep_duration);
	}
	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_end);

	printf(
		"When required to sleep %.4lf us, usleep actually sleeps %.4lf us\n",
		1e6 * total_duration / NB_ITER,
		timespec2usec(timespec_diff(&timespec_begin, &timespec_end)) / (NB_ITER+1)
	);
}

int main(int argc, char* argv[])
{
	struct timespec timespec_begin, timespec_end, timespec;


	/* clock_gettime */

	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_begin);
	for (int i = 0; i < NB_ITER; i++)
	{
		clock_gettime(CLOCK_MONOTONIC_RAW, &timespec);
	}
	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_end);

	printf(
		"One call to clock_gettime takes %.4lf us\n",
		timespec2usec(timespec_diff(&timespec_begin, &timespec_end)) / (NB_ITER+1)
	);


	/* rdtsc */

	uint64_t t;
	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_begin);
	for (int i = 0; i < NB_ITER; i++)
	{
		t = rdtsc_get_tick();
	}
	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_end);

	printf(
		"One call to rdtsc takes %.4lf us (dummy value : %lu)\n",
		timespec2usec(timespec_diff(&timespec_begin, &timespec_end)) / (NB_ITER+1),
		t
	);


	/* nano_sleep */

	struct timespec nano_sleep_spec, nano_sleep_rem;
	nano_sleep_spec.tv_sec = 0;
	nano_sleep_spec.tv_nsec = 0;
	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_begin);
	for (int i = 0; i < NB_ITER; i++)
	{
		nanosleep(&nano_sleep_spec, &nano_sleep_rem);
	}
	clock_gettime(CLOCK_MONOTONIC_RAW, &timespec_end);

	printf(
		"One call to zero nano_sleep takes %.4lf us\n",
		timespec2usec(timespec_diff(&timespec_begin, &timespec_end)) / (NB_ITER+1)
	);


	/* overhead of different sleep */

	overhead_precise_sleep(10);
	overhead_precise_sleep(1);
	overhead_precise_sleep(0.1);
	overhead_precise_sleep(0.01);
	overhead_precise_sleep(0.001);
	overhead_precise_sleep(0.0001);
	overhead_precise_sleep(0.00001);
	overhead_precise_sleep(0.000001);

	overhead_very_precise_sleep(10);
	overhead_very_precise_sleep(1);
	overhead_very_precise_sleep(0.1);
	overhead_very_precise_sleep(0.01);
	overhead_very_precise_sleep(0.001);
	overhead_very_precise_sleep(0.0001);
	overhead_very_precise_sleep(0.00001);
	overhead_very_precise_sleep(0.000001);

	overhead_usleep(10);
	overhead_usleep(1);
	overhead_usleep(0.1);
	overhead_usleep(0.01);
	overhead_usleep(0.001);
	overhead_usleep(0.0001);
	overhead_usleep(0.00001);
	overhead_usleep(0.000001);


	/* resolution of clock */

	if (clock_getres(CLOCK_MONOTONIC_RAW, &timespec) == -1)
	{
		perror("clock_getres");
		return EXIT_FAILURE;
	}
	printf("Resolution of CLOCK_MONOTONIC_RAW: %.4lf us\n", timespec2usec(timespec));

	return EXIT_SUCCESS;
}
