#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	const char* const str = "foo=bar+baz\n";

	char str0[64];
	char str1[64];
	char str2[64];

	int ret = sscanf(str, "%63[^=]=%63[^+]+%63[^\n]\n", str0, str1, str2);
	// int ret = sscanf(str, "%[^=]=%[^+]+%[^\n]\n", str0, str1, str2);

	printf("ret = %d\n", ret);
	printf("str0 = %s\n", str0);
	printf("str1 = %s\n", str1);
	printf("str2 = %s\n", str2);

	return EXIT_SUCCESS;
}
