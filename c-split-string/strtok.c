#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void parse(char* str)
{
	printf("Parsing '%s'\n", str);

	char *sub, *save;
	char delim[] = "|,";

	sub = strtok_r(str, delim, &save);
	for (; sub != NULL; sub = strtok_r(NULL, delim, &save))
	{
		printf("\t%s\n", sub);
	}
}

int main()
{
	char* str = strdup("a|b,c|d"); // the string to manipulate can't be in the .text section of the binary.
	parse(str);
	free(str);

	str = strdup("abcd"); // the string to manipulate can't be in the .text section of the binary.
	parse(str);
	free(str);

	return EXIT_SUCCESS;
}
