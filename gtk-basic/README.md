From https://www.gtk.org/docs/getting-started/hello-world/


## On MacOS

With Homebrew, install `zlib`, `gtk+3` and `adwaita-icon-theme` (to display icons). Then, adapt the `PKG_CONFIG_PATH` variable:

```bash
export PKG_CONFIG_PATH="/usr/local/opt/gdk-pixbuf/lib/pkgconfig:/usr/local/opt/zlib/lib/pkgconfig:/usr/local/opt/libpng/lib/pkgconfig:/usr/local/opt/pixman/lib/pkgconfig:/usr/local/opt/cairo/lib/pkgconfig:/usr/local/opt/fontconfig/lib/pkgconfig:/usr/local/opt/fribidi/lib/pkgconfig:/usr/local/opt/graphite2/lib/pkgconfig:/usr/local/opt/freetype2/lib/pkgconfig:/usr/local/opt/harfbuzz/lib/pkgconfig:/usr/local/opt/pcre/lib/pkgconfig:/usr/local/opt/glib/lib/pkgconfig:/usr/local/opt/pango/lib/pkgconfig:/usr/local/opt/atk/lib/pkgconfig:/usr/local/opt/libepoxy/lib/pkgconfig:/usr/local/opt/gtk+3/lib/pkgconfig:$PKG_CONFIG_PATH"
```


## On Debian

Install package `libgtk-3-dev`.


## On Windows

Follow https://www.gtk.org/docs/installations/windows/#using-gtk-from-msys2-packages

With MSYS2, install package `mingw-w64-x86_64-gtk3` and build with `gcc` like on Unix-like systems, just add the flag `-mwindows` to tell GCC to hide the shell window. See https://wiki.ph-sw.fr/doku.php?id=c:windows#compiler_un_projet_gtk for information about how to distribute the program.



## Building

```bash
gcc basic.c `pkg-config --cflags --libs gtk+-3.0`
```



## Internationalization and Localization

See `i18n.c` example.

http://ding-hao.blogspot.com/2012/07/internationalizationlocalization-in-gtk.html

To generate translation files:
```bash
mkdir po
xgettext --sort-output --keyword=_ --no-wrap --escape -o po/msg.pot i18n.c
```

Copy `msg.pot` to `fr.pot` and translate the file. The header of the file should look like:
```
"Language: fr\n"
"Content-Type: text/plain; charset=UTF-8\n"
```

```bash
msgfmt fr.pot -o fr.mo
sudo cp fr.mo /usr/share/locale/fr/LC_MESSAGES/demo.mo
# /usr/share/locale is LOCALE_DIR define in code
# demo is PACKAGE define in code
```

Then the `LC_ALL` environment variable defines the selected language:
```bash
LC_ALL="C" ./i18n # code version
LC_ALL="fr_FR.UTF8" ./i18n # french version
# but LC_ALL is probably already defined according to user configuration
```

## With CMake

```bash
mkdir build
cd build
cmake ..
make
./bin/basic
```

