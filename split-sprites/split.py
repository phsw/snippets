# Mostly inspired from https://stackoverflow.com/questions/41006278/pillow-splitting-a-spritesheet-into-seprate-images
from PIL import Image


DIM = 32


sheet = Image.open("spritesheet.png")
count = 1

for x in range(6):
    for y in range(6):
        # (0, 0) is upper left
        # (left, upper, right, lower)
        icon = sheet.crop((DIM*x, DIM*y, DIM*(x+1), DIM*(y+1)))
        icon.save(f"{count}.png")
        count += 1
