# To be executed piped to cat

import subprocess


def exec_command(cmd: str) -> int:
    execution = subprocess.run(cmd, text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    if len(execution.stdout.rstrip()) > 0:
        print(execution.stdout.rstrip())

    return execution.returncode


print("Print avant")
print(exec_command("echo -n 'foo'"))
print("Print après")
