#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
    printf("Parent process\n");

    pid_t pid = fork();
    if (pid == -1)
    {
        printf("Error in fork\n");
        return EXIT_FAILURE;
    }

    if (pid == 0)
    {
        printf("I'm the new child process\n");
    }
    else
    {
        printf("Still the old parent process\n");
    }

    sleep(10);

    return EXIT_SUCCESS;
}
