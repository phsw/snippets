#include <stdlib.h>
#include <stdio.h>

#define FORMAT "%04d"

int main()
{
	int value = 42;

	printf("Value: "FORMAT"\n", value);

	return EXIT_SUCCESS;
}
