/* Progam that gets an ID for each node (ie for a different ID for each
 * different hostname).
 *
 * From https://stackoverflow.com/questions/35626377/get-nodes-with-mpi-program-in-c */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>


int main(int argc, char **argv)
{
	int world_rank, world_size;

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	MPI_Barrier(MPI_COMM_WORLD);

	char hostname[65];
	gethostname(hostname, sizeof(hostname));

	MPI_Comm intra_node_comm;
	MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, world_rank, MPI_INFO_NULL, &intra_node_comm);
	int intra_node_comm_rank;
	MPI_Comm_rank(intra_node_comm, &intra_node_comm_rank);
	MPI_Comm_free(&intra_node_comm);

	MPI_Comm inter_node_comm;
	MPI_Comm_split(MPI_COMM_WORLD, intra_node_comm_rank, world_rank, &inter_node_comm);
	int inter_node_comm_size;
	int inter_node_comm_rank;
	MPI_Comm_size(inter_node_comm, &inter_node_comm_size);
	MPI_Comm_rank(inter_node_comm, &inter_node_comm_rank);
	MPI_Comm_free(&inter_node_comm);

	int nb_nodes_from_0 = inter_node_comm_size;
	MPI_Bcast(&nb_nodes_from_0, 1, MPI_INT, 0, MPI_COMM_WORLD);
	if (nb_nodes_from_0 != inter_node_comm_size)
	{
		fprintf(stderr, "Warning: all MPI processes did not compute the same number of nodes. Your process mapping is probably unbalanced.\n");
	}

	printf("[%s] world rank = %d; intra_node_comm_rank = %d; inter_node_comm_size = nb nodes = %d; inter_node_comm_size = node_id = %d\n", hostname, world_rank, intra_node_comm_rank, inter_node_comm_size, inter_node_comm_rank);

	MPI_Finalize();

	return EXIT_SUCCESS;
}
