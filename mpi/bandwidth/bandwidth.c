#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>

#include "timing.h"


#define NB_PINGPONGS 50
#define BUFFER_NB_ITEMS 16777216 // 64 MB is enough to see impact on network bandwidth

double ping_pong(int rank, float* send_buffer, float* recv_buffer)
{
    puk_tick_t start_time, end_time;
    double duration = 0;

    if (rank == 0)
    {
	PUK_GET_TICK(start_time);
	MPI_Send(send_buffer, BUFFER_NB_ITEMS, MPI_FLOAT, 1, 1, MPI_COMM_WORLD);
	MPI_Recv(recv_buffer, BUFFER_NB_ITEMS, MPI_FLOAT, 1, 2, MPI_COMM_WORLD, NULL);
	PUK_GET_TICK(end_time);

	duration = PUK_TIMING_DELAY(start_time, end_time) / 2;
    }
    else
    {
	MPI_Recv(recv_buffer, BUFFER_NB_ITEMS, MPI_FLOAT, 0, 1, MPI_COMM_WORLD, NULL);
	MPI_Send(send_buffer, BUFFER_NB_ITEMS, MPI_FLOAT, 0, 2, MPI_COMM_WORLD);
    }

    return duration;
}

int main(int argc, char* argv[])
{
    int rank, worldsize;
    double durations[NB_PINGPONGS];

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &worldsize);

    if (rank == 0)
    {
	printf("# Starting...\n");
	fflush(stdout);
    }

    float* send_buffer = malloc(BUFFER_NB_ITEMS * sizeof(float));
    float* recv_buffer = malloc(BUFFER_NB_ITEMS * sizeof(float));

    for (int i = 0; i < BUFFER_NB_ITEMS; i++)
    {
	send_buffer[i] = 3.14;
    }
    memset(recv_buffer, 0, BUFFER_NB_ITEMS * sizeof(float));

    for (int i = 0; i < NB_PINGPONGS; i++)
    {
	durations[i] = ping_pong(rank, send_buffer, recv_buffer);
    }

    free(send_buffer);
    free(recv_buffer);

    if (rank == 0)
    {
	for (int i = 0; i < NB_PINGPONGS; i++)
	{
	    printf("%.3lf MB/s\n", BUFFER_NB_ITEMS * sizeof(float) / (1024*1024*(durations[i] / 1000000)));
	}
    }

    fflush(stdout);

    MPI_Finalize();

    return EXIT_SUCCESS;
}
