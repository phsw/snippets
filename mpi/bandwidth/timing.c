/* From PM2's Puk timing interface */
#include "timing.h"

#define PUK_TICK_RAW_DIFF(start, end) puk_tick_raw_diff(&(start), &(end))


static inline puk_tick_t puk_tick_raw_diff(const puk_tick_t*const t1, const puk_tick_t*const t2)
{
    puk_tick_t diff;
    if(t2->tv_nsec > t1->tv_nsec)
    {
	diff.tv_sec  = t2->tv_sec - t1->tv_sec;
	diff.tv_nsec = t2->tv_nsec - t1->tv_nsec;
    }
    else
    {
	diff.tv_sec  = t2->tv_sec - t1->tv_sec - 1;
	diff.tv_nsec = t2->tv_nsec - t1->tv_nsec + 1000000000L;
    }
    return diff;
}

/** converts a tick diff to microseconds */
static inline double puk_tick2usec_raw(puk_tick_t t)
{
    double raw_delay;

    raw_delay = 1000000.0 * t.tv_sec + t.tv_nsec / 1000.0;

    return raw_delay;
}

/** converts a tick diff to microseconds, with offset compensation */
double puk_tick2usec(puk_tick_t t)
{
    static double clock_offset = -1.0;
    if(clock_offset < 0)
    {
	/* lazy clock offset init */
	int i;
	for(i = 0; i < 1000; i++)
	{
	    puk_tick_t t1, t2;
	    PUK_GET_TICK(t1);
	    PUK_GET_TICK(t2);
	    const double raw_delay = puk_tick2usec_raw(PUK_TICK_RAW_DIFF(t1, t2));
	    if(raw_delay < clock_offset || clock_offset < 0)
		clock_offset = raw_delay;
	}
    }
    const double delay = puk_tick2usec_raw(t) - clock_offset;
    return (delay > 0.0) ? delay : 0.0; /* tolerate slight offset inacurracy */
}

/** computes a delay between ticks, in microseconds */
double puk_ticks2delay(const puk_tick_t*t1, const puk_tick_t *t2)
{
    puk_tick_t tick_diff = PUK_TICK_RAW_DIFF(*t1, *t2);
    return puk_tick2usec(tick_diff);
}
