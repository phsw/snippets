#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>


int main(int argc, char* argv[])
{
    int rank, worldsize;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &worldsize);

    MPI_Bcast(NULL, 0, MPI_INT, 0, MPI_COMM_WORLD);

    printf("Hello from rank %d ( / %d processes)\n", rank, worldsize);

    MPI_Finalize();

    return EXIT_SUCCESS;
}
