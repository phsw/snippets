#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>


int main(int argc, char* argv[])
{
    int rank, worldsize;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &worldsize);

    printf("Hello from rank %d ( / %d processes)\n", rank, worldsize);

	int value = rank;
	int* values = (rank == 0 ? malloc(worldsize*sizeof(int)) : NULL);

	MPI_Gather(&value, 1, MPI_INT, values, 1, MPI_INT, 0, MPI_COMM_WORLD);

	if (rank == 0)
	{
		printf("Rank 0 collected these values: ");
		for (int i = 0; i < worldsize; i++)
		{
			printf("%d ", values[i]);
		}
		printf("\n");
		free(values);
	}

    MPI_Finalize();

    return EXIT_SUCCESS;
}
