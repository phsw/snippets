// Inspired from https://mpitutorial.com/tutorials/introduction-to-groups-and-communicators/

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <assert.h>


int main(int argc, char* argv[])
{
	int rank, worldsize, nb_nodes, i, sub_group_rank;

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &worldsize);

	printf("Hello from rank %d ( / %d processes)\n", rank, worldsize);

	MPI_Group world_group;
	MPI_Comm_group(MPI_COMM_WORLD, &world_group);

	for (nb_nodes = 2; nb_nodes <= worldsize; nb_nodes++)
	{
		MPI_Barrier(MPI_COMM_WORLD);

		if (rank == 0)
		{
			printf("Nb nodes: %d\n", nb_nodes);
		}

		if (rank >= nb_nodes)
		{
			continue;
		}

		int* group_ranks = malloc(nb_nodes * sizeof(int));
		for (i = 0; i < nb_nodes; i++)
		{
			group_ranks[i] = i;
		}

		MPI_Group sub_group;
		MPI_Group_incl(world_group, nb_nodes, group_ranks, &sub_group);

		MPI_Comm sub_comm;
		MPI_Comm_create_group(MPI_COMM_WORLD, sub_group, 0, &sub_comm);

		/* True, since nodes not in the group are blocked by the previous
		 * condition rank >= nb_nodes */
		assert(sub_comm != MPI_COMM_NULL);

		MPI_Comm_rank(sub_comm, &sub_group_rank);
		printf("I'm rank %d in the world, but rank %d in the sub_group\n", rank, sub_group_rank);

		MPI_Group_free(&sub_group);
		MPI_Comm_free(&sub_comm);

		free(group_ranks);
	}

	MPI_Group_free(&world_group);
	MPI_Finalize();

	return EXIT_SUCCESS;
}
