#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>


int main(int argc, char* argv[])
{
	int rank, worldsize;

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &worldsize);

	printf("Hello from rank %d ( / %d processes)\n", rank, worldsize);

	void* attr_value_p = NULL;
	int attr_value = -1;
	int flag = -1;
	MPI_Comm_get_attr(MPI_COMM_WORLD, MPI_WTIME_IS_GLOBAL, &attr_value_p, &flag);

	printf("MPI_WTIME_IS_GLOBAL: value=%d flag=%d\n", *((int*) attr_value_p), flag);

	MPI_Finalize();

	return EXIT_SUCCESS;
}
