import argparse
import re

from matplotlib import pyplot as plt
import matplotlib.patches as patches
from matplotlib.colors import LogNorm


class CommMatrix:
    def __init__(self, input_files, nb_cores_per_node=None):
        self.nb_processes = len(input_files)
        self.matrix_nb_msgs = [[0 for _ in range(self.nb_processes)] for _ in range(self.nb_processes)]
        self.matrix_data_volume = [[0 for _ in range(self.nb_processes)] for _ in range(self.nb_processes)]
        if nb_cores_per_node is not None:
            self.nb_nodes = self.nb_processes // nb_cores_per_node
            self.node_matrix_nb_msgs = [[0 for _ in range(self.nb_nodes)] for _ in range(self.nb_nodes)]
            self.node_matrix_data_volume = [[0 for _ in range(self.nb_nodes)] for _ in range(self.nb_nodes)]

        pattern = re.compile(r"E\s+(\d+)\s+(\d+)\s+(\d+) bytes\s+(\d+)")

        for filename in input_files:
            with open(filename, "r") as f:
                for line in f:
                    matches = pattern.match(line)
                    if matches is not None:
                        sender = int(matches.group(1))
                        receiver = int(matches.group(2))
                        size = int(matches.group(3))
                        nb_msgs = int(matches.group(4))

                        self.matrix_nb_msgs[receiver][sender] += nb_msgs
                        self.matrix_data_volume[receiver][sender] += size

                        if nb_cores_per_node is not None:
                            self.node_matrix_nb_msgs[receiver // nb_cores_per_node][sender // nb_cores_per_node] += nb_msgs
                            self.node_matrix_data_volume[receiver // nb_cores_per_node][sender // nb_cores_per_node] += size


cli_parser = argparse.ArgumentParser()
cli_parser.add_argument("--display", action=argparse.BooleanOptionalAction, help="Wether to display or not the figure", default=True)
cli_parser.add_argument("--nb-cores", help="Number of cores per node", type=int)
cli_parser.add_argument("input_files", help="Input files of results to plot", nargs="+")
args = cli_parser.parse_args()

comm_matrix = CommMatrix(args.input_files, args.nb_cores)

fig, ax = plt.subplots()
im = ax.imshow(comm_matrix.matrix_nb_msgs, interpolation=None, cmap="Greys") #, norm=LogNorm())
if args.nb_cores is not None:
    assert(comm_matrix.nb_processes % args.nb_cores == 0)
    for node in range(comm_matrix.nb_processes // args.nb_cores):
        ax.add_patch(patches.Rectangle(
            (-0.5+node*args.nb_cores, -0.5+node*args.nb_cores),
            args.nb_cores, args.nb_cores,
            edgecolor='red',
            fill=False,
            lw=0.5
        ))
ax.set(title="Number of messages")
fig.colorbar(im)
if args.display:
    plt.show()
else:
    plt.savefig("matrix-nb.png")

fig, ax = plt.subplots()
im = ax.imshow(comm_matrix.matrix_data_volume, interpolation=None, cmap="Greys") #, norm=LogNorm())
if args.nb_cores is not None:
    assert(comm_matrix.nb_processes % args.nb_cores == 0)
    for node in range(comm_matrix.nb_processes // args.nb_cores):
        ax.add_patch(patches.Rectangle(
            (-0.5+node*args.nb_cores, -0.5+node*args.nb_cores),
            args.nb_cores, args.nb_cores,
            edgecolor='red',
            fill=False,
            lw=0.5
        ))
ax.set(title="Volume of exchanged data")
fig.colorbar(im)
if args.display:
    plt.show()
else:
    plt.savefig("matrix-volume.png")
