# Monitoring MPI communications with OpenMPI, by decomposing collectives

[Online Dynamic Monitoring of MPI Communications: Scientific User and
Developper Guide](https://inria.hal.science/hal-01485243v1/file/RR-9038.pdf)

```sh
export OMPI_MCA_pml_monitoring_enable=1
export OMPI_MCA_pml_monitoring_enable_output=3
export OMPI_MCA_pml_monitoring_filename=prefix

mpirun ...
```

It will generate one file per process.
