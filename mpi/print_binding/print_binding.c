#define _GNU_SOURCE // for c99 and gethostname()

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <mpi.h>

#ifdef WITH_HWLOC
#include <hwloc.h>
#endif // WITH_HWLOC

void get_binding(char* binding_str)
{
    char hostname[128];
    gethostname(hostname, sizeof(hostname));

    strcpy(binding_str, hostname);

#ifdef WITH_HWLOC
    hwloc_topology_t topology;
    hwloc_bitmap_t cpuset = hwloc_bitmap_alloc();

    hwloc_topology_init(&topology);
    hwloc_topology_load(topology);

    int ret = hwloc_get_cpubind(topology, cpuset, HWLOC_CPUBIND_THREAD);
    if (ret)
    {
        fprintf(stderr, "Error: could not get current CPU binding: %s\n", strerror(errno));
        goto end;
    }

    int n_cores = hwloc_get_nbobjs_inside_cpuset_by_type(topology, cpuset, HWLOC_OBJ_CORE);
    if (n_cores > 1)
    {
        fprintf(stderr, "Warning, the process is bound on %d cores\n", n_cores);
    }
    else
    {
        char* object_type_str;
        hwloc_obj_type_t obj_type;

        if (n_cores == 0)
        {
            /* No core: bound to a hyperthread */
            object_type_str = "PU";
            obj_type = HWLOC_OBJ_PU;
        }
        else
        {
            object_type_str = "Core";
            obj_type = HWLOC_OBJ_CORE;
        }
        struct hwloc_location loc;
        hwloc_obj_t numa_nodes[2];
        unsigned n_numa_parent_core = 2;
        loc.type = HWLOC_LOCATION_TYPE_OBJECT;

        hwloc_obj_t obj = hwloc_get_obj_inside_cpuset_by_type(topology, cpuset, obj_type, 0);
        if (obj == NULL)
        {
            fprintf(stderr, "Error: no PU or core was found\n");
            goto end;
        }

        loc.location.object = obj;
        hwloc_get_local_numanode_objs(topology, &loc, &n_numa_parent_core, numa_nodes, HWLOC_LOCAL_NUMANODE_FLAG_LARGER_LOCALITY);
        if (n_numa_parent_core > 1)
        {
            fprintf(stderr, "Warning: the core is in several NUMA nodes\n");
        }

        hwloc_obj_t package_obj = hwloc_get_ancestor_obj_by_type(topology, HWLOC_OBJ_PACKAGE, obj);
        sprintf(binding_str, "%s, on %s %3d belonging to NUMA node %2d belonging to package %d", hostname, object_type_str, obj->logical_index, numa_nodes[0]->logical_index, package_obj->logical_index);
    }

end:
    hwloc_topology_destroy(topology);
    hwloc_bitmap_free(cpuset);
#endif// WITH_HWLOC
}


int main(int argc, char* argv[])
{
    int rank, worldsize;

    char binding_str[1024] = { 0 };

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &worldsize);

    get_binding(binding_str);

    for (int i = 0; i < worldsize; i++)
    {
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == i)
        {

            printf("Hello from rank %3d (out of %d processes) bound on %s\n", rank, worldsize, binding_str);
            fflush(stdout);
        }
    }

    MPI_Finalize();

    return EXIT_SUCCESS;
}
