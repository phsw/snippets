// From https://stackoverflow.com/a/31722775

/* How to know if Intel's turboboost is enabled:
 * cat /sys/devices/system/cpu/intel_pstate/no_turbo
 * 0 => turbo boost on, off otherwise
 * (from https://askubuntu.com/a/991839)
 */

#include <stdio.h>

double get_cpu_freq() {
    FILE *cmd = popen("grep 'pu MHz' /proc/cpuinfo | cut -d : -f 2 | awk '{ total += $1; count++ } END { print total/count }'", "r");

    if (cmd == NULL)
        return -1;

    size_t n;
    char buff[8];
    double cpu_freq = -1;

    if ((n = fread(buff, 1, sizeof(buff)-1, cmd)) <= 0)
        return -1;

    buff[n] = '\0';
    if (sscanf(buff, "%lf", &cpu_freq) != 1)
        return -1;

    pclose(cmd);

    return cpu_freq;
}

double get_cpu_freq_core(int core_os_index)
{
    char filepath[255];
    sprintf(filepath, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq", core_os_index);

    FILE* freq_file = fopen(filepath, "r");
    if (freq_file == NULL)
    {
        return -1;
    }

    size_t n;
    char buff[8];
    int cpu_freq = -1;

    if ((n = fread(buff, 1, sizeof(buff)-1, freq_file)) <= 0)
        return -1;

    buff[n] = '\0';
    if (sscanf(buff, "%d", &cpu_freq) != 1)
        return -1;

    fclose(freq_file);

    return ((double) cpu_freq) / 1000.0f;
}

int main(void) {
    double cpu_freq = get_cpu_freq();
    if (cpu_freq == -1)
        fprintf(stderr, "Error retrieving cpu freq\n");
    else
        printf("AVG CPU freq: %lf\n", cpu_freq);


    cpu_freq = get_cpu_freq_core(0);
    if (cpu_freq == -1)
        fprintf(stderr, "Error retrieving cpu freq\n");
    else
        printf("CPU0 freq: %lf\n", cpu_freq);

    return 0;
}

