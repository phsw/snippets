#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <likwid.h>


static int sleeptime = 1;

static int run = 1;

void  INThandler(int sig)
{
    signal(sig, SIG_IGN);
    run = 0;
}


int main (int argc, char* argv[])
{
    int c;
    topology_init();
    CpuTopology_t cputopo = get_cpuTopology();

    signal(SIGINT, INThandler);


    while (run)
    {
        sleep(sleeptime);

        for (c = 0; c < cputopo->numHWThreads; c++)
        {
            printf("cpu=%d freq = %lu MHz\n", c, freq_getCpuClockCurrent(c) / 1000);
        }

        for (c = 0; c < cputopo->numSockets; c++)
        {
            printf("core=%d uncore freq = %lu MHz\n", c, freq_getUncoreFreqCur(c));
        }
        printf("\n");
    }

monitor_exit:
    topology_finalize();
    return 0;
}
