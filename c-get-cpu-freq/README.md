## get-cpu-freq

This code illustrates how to get current average frequency of all cores, by executing a shell command, which reads content of `/proc/cpuinfo`.

So this code is also an example of usage of `popen()`.

## likwid-freq

This code illustrates how to get current frequency of a given core and how to get the uncore frequency of a given socket with the library [likwid](https://hpc.fau.de/research/tools/likwid/) 

**Warning**: do not use PAPI and likwid simultaneously ! It will make counter values inconsistent.
