# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
# https://gsalvatovallverdu.gitlab.io/python/curve_fit/
import numpy as np
from scipy.optimize import curve_fit


class CurveFit:
    def __init__(self, f, x, y, y_std=None):
        """
        f: the function to fit, for instance: lambda x, a, b, c: a*x**2+b*x+c
        x: list of x values
        y: list of y values
        y_std: list of standard deviation for each y point (computed with np.std() for instance)
        """
        self.x = np.array(x)
        self.y = np.array(y)
        self.f = f
        self.y_std = y_std

        if self.y_std is None:
            self.coefs, _ = curve_fit(self.f, self.x, self.y)
        else:
            self.coefs, _ = curve_fit(self.f, self.x, self.y, sigma=self.y_std, absolute_sigma=True)

    @property
    def r_square(self):
        # https://stackoverflow.com/questions/19189362/getting-the-r-squared-value-using-curve-fit/37899817
        residuals = np.array([self.y[i] - self.f(self.x[i], *self.coefs) for i in range(len(self.x))])
        ss_res = np.sum(residuals**2)
        ss_tot = np.sum((self.y-np.mean(self.y))**2)

        return 1 - (ss_res / ss_tot)


x = [0, 1, 2, 3, 4]

y = [0, 1, 4, 9, 15.5]
c = CurveFit(lambda x, a, b, c : a*x**2+b*x+c, x, y)
print(c.coefs, "(should be close to [1, 0, 0])")
print(c.r_square, "(should be close to 1)")


y = [1, 3, 5, 7.2, 9]
c = CurveFit(lambda x, a, b : a*x+b, x, y)
print(c.coefs, "(should be close to [2, 1])")
print(c.r_square, "(should be close to 1)")
