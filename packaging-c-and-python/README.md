# Packaging C application and Python package with autotools


... what a nightmare!


We have a C program, printing some stuff on the standard output and a Python
script to analyze this output. The Python script uses a Python package we made
(in plain Python). The goal is to package and distribute this whole project:
- the C program
- the Python script
- the Python package, to be callable by other Python scripts
- bonus: create a Guix package

The folder hierarchy is the following:
```
.
├── python
│   ├── ph_prog # the Python package
│   │   ├── __init__.py
│   │   └── output_analyzer.py
│   └── ph_prog_analyze # the Python script
└── src
    └── ph_prog.c # the C program
```


## Packaging the C program with autotools

Nothing special here, a basic autotools system.



## Packaging the Python script

Simply distribute the `ph_prog_analyze` script:
```
dist_bin_SCRIPTS = ph_prog_analyze
```

## Packaging the Python module

Sources:
- https://packaging.python.org/en/latest/tutorials/packaging-projects/
- https://blog.kevin-brown.com/programming/2014/09/24/combining-autotools-and-setuptools.html

We start with the basic packaging of a Python module, which requires especially
the files `pyproject.toml` and `setup.cfg`. We deal here with the integration
with autotools.

Test if Python is available in `configure.ac`:
```sh
AM_PATH_PYTHON([3.6],, [:])

install_python_package=yes
if test "$PYTHON" != :
then
    # We will need the Python package name in Makefiles, so define it here:
    PYTHON_PKG_NAME=ph_prog
    AC_SUBST([PYTHON_PKG_NAME])

    AC_CONFIG_FILES([python/setup.cfg])
else
    install_python_package=no
fi

AM_CONDITIONAL(INSTALL_PYTHON_MODULE, test x$install_python_package = xyes)
AC_MSG_CHECKING(whether Python package can be installed)
AC_MSG_RESULT($install_python_package)
```

Tools building Python packages create a `*.egg-info` folder into package
sources, so we copy the source files to the build folder, to avoid writing in
the source folder:
```
all-local:
    cp $(srcdir)/pyproject.toml .
    cp -R $(srcdir)/$(PYTHON_PKG_NAME) .
```

To manage the Python package, we use the `pip` module, so check in
`configure.ac` this module is available:
```sh
# From starpu/m4/libs.m4, could go into a m4/ file
# AC_PYTHON_MODULE(modulename, [action-if-found], [action-if-not-found])
# Check if the given python module is available
AC_DEFUN([AC_PYTHON_MODULE],
[
	echo "import $1" | $PYTHON - 2>/dev/null
	if test $? -ne 0 ; then
		$3
	else
		$2
	fi
])

AM_PATH_PYTHON([3.6],, [:])

install_python_package=yes
if test "$PYTHON" != :
then
	AC_MSG_CHECKING(for python3 pip module)
	AC_PYTHON_MODULE(pip, [have_python_pip=yes], [have_python_pip=no install_python_package=no])
	AC_MSG_RESULT($have_python_pip)

	if test x$install_python_package = xyes
	then
		PYTHON_PKG_NAME=ph_prog
		AC_SUBST([PYTHON_PKG_NAME])

		AC_CONFIG_FILES([python/setup.cfg])
	fi
else

```
Commands to install and uninstall the package:
```
install-exec-local:
    $(PYTHON) -m pip install --upgrade --target $(pythondir) .

uninstall-local:
    PYTHONPATH=$(pythondir):$(PYTHONPATH) $(PYTHON) -m pip uninstall --yes $(PYTHON_PKG_NAME)
```

- `--upgrade` to force the reinstallation, even if the package is already
  installed
- `--target $(pythondir)` to install the package in the `--prefix` given by
  the user to `configure`
- `PYTHONPATH=$(pythondir):$(PYTHONPATH)` to let `pip` find the package to
  uninstall even if it was installed in a uncommon place
- `--yes` to disable a prompt asking for confirmation to uninstall the package

This will work with a custom installation prefix:
```sh
./autogen.sh
mkdir build
cd build
../configure --prefix=/home/user/install
make
make install
/home/user/install/bin/ph_prog | PYTHONPATH=/home/user/install/lib/python3.9/site-packages:$PYTHONPATH ph_prog_analyze
make uninstall
```

However, with a default installation without a custom `--prefix`, the Python
package will be installed in `/usr/local/lib/python3.9/site-packages`, which is
not in the default module search paths of Python (to list them: `python3 -c
'import sys; print("\n".join(sys.path))'`). Hence we have to distinguish wether
we are installing in a custom location or not:
```
AM_CONDITIONAL(HAS_CUSTOM_INSTALL_PREFIX, test x${prefix} != xNONE)
```
```
install-exec-local:
if HAS_CUSTOM_INSTALL_PREFIX
	$(PYTHON) -m pip install --upgrade --target $(pythondir) .
else
	$(PYTHON) -m pip install --upgrade --system .
endif

uninstall-local:
if HAS_CUSTOM_INSTALL_PREFIX
	PYTHONPATH=$(pythondir):$(PYTHONPATH) $(PYTHON) -m pip uninstall --yes $(PYTHON_PKG_NAME)
else
	$(PYTHON) -m pip uninstall --yes $(PYTHON_PKG_NAME)
endif
```

Now it works also with the default prefix:
```sh
./autogen.sh
cd build
../configure
make
sudo make install
ph_prog | ph_prog_analyze
sudo make uninstall
```

Note that the folder hierarchy we chose for the `python` folder (the other
possibility is to put the folder `ph_prog` in a `src` folder) allows to use our
Python module directly from the sources, without having to build/install it after
every change, or playing with environment variables (useful for development and
debuging!):
```sh
cd python
../build/src/ph_prog | ./ph_prog_analyze
```




## Guix package

This package is a little bit tricky in the sense it's a mix of two building
systems: the `gnu-build-system` (for autotools) and `python-build-system` (for
the Python package). However the main system here is autotools, so the plan was
to use a `gnu-build-system` and add required Python packages as
build-dependencies to be able to also install the Python package.

At the time when I was testing the Guix packaging, the project wasn't
versionned or available online yet. However, we can't (or I don't know how -
more likely) define a Guix package without a source, or with the current
directory as the source. The solution I used was to fake the source field (as
well as other mandatory fields I didn't care about at that time) by using those
of the `hello` package:
```scheme
(define-public c-python
 (package
  (name "c-python")
  (version "0.1")
  (source (origin
           (method url-fetch)
           (uri (string-append "mirror://gnu/hello/hello-2.10.tar.gz"))
           (sha256
            (base32
             "0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i"))))
  (build-system gnu-build-system)
  (native-inputs (list python-pip python-setuptools python-wheel))
  (synopsis "Hello, GNU world: An example GNU package")
  (description "Guess what GNU Hello prints!")
  (home-page "https://www.gnu.org/software/hello/")
  (license gpl3+)))
```

And now I can use a package transformation to indicate the real sources:
```sh
guix build -L . c-python --with-source=c-python=$(pwd)
```
Note we can't use `.` instead of `$(pwd)`:
```sh
guix build -L . c-python --with-source=c-python=.
guix build: erreur : invalid name: `.'
```

Everything seems alright, however our Python package isn't installed. Indeed:
```
checking for a Python interpreter with version >= 3.6... none
```
Ok, so `python-{pip,setuptools,wheel}` as native inputs do not imply Python as
a native dependency... Let's fix it:
```scheme
(native-inputs (list python python-pip python-setuptools python-wheel))
```

And now, new error:
```
cp ./pyproject.toml .
cp: './pyproject.toml' and './pyproject.toml' are the same file
make[1]: *** [Makefile:481: all-local] Error 1
```
In Guix, by default the build is done in the source folder; this is not
supported with the copy of Python sources we introduced previously.
Fortunately, there is an option to disable such behaviour:
```scheme
(arguments '(#:out-of-source? #t))
```

New error:
```
  Installing build dependencies: started
  Installing build dependencies: finished with status 'error'
  ERROR: Command errored out with exit status 1:
   command: /gnu/store/p5fgysbcnnp8b1d91mrvjvababmczga0-python-3.9.6/bin/python3 /tmp/guix-build-c-python-0.1.drv-0/pip-standalone-pip-3ivenp5m/__env_pip__.zip/pip install --ignore-installed --no-user --prefix /tmp/guix-build-c-python-0.1.drv-0/pip-build-env-gahj7cay/overlay --no-warn-script-location --no-binary :none: --only-binary :none: -i https://pypi.org/simple -- 'setuptools>=42' wheel
       cwd: None
  Complete output (8 lines):
  WARNING: The directory '/homeless-shelter/.cache/pip' or its parent directory is not owned or is not writable by the current user. The cache has been disabled. Check the permissions and owner of that directory. If executing pip with sudo, you should use sudo's -H flag.
  WARNING: Retrying (Retry(total=4, connect=None, read=None, redirect=None, status=None)) after connection broken by 'NewConnectionError('<pip._vendor.urllib3.connection.HTTPSConnection object at 0x7ffff5c16af0>: Failed to establish a new connection: [Errno -2] Name or service not known')': /simple/setuptools/
  ...
  ERROR: Could not find a version that satisfies the requirement setuptools>=42 (from versions: none)
  ERROR: No matching distribution found for setuptools>=42
```

Let's enable the verbose ouput of `pip`:
```sh
$(PYTHON) -m pip install -v --upgrade --target $(pythondir) .
```

Hmm, the `-v` doesn't appear in Guix logs:
```
/gnu/store/p5fgysbcnnp8b1d91mrvjvababmczga0-python-3.9.6/bin/python3 -m pip install --upgrade --target /gnu/store/2h38x76kyljp6ai1y7glpqqmdi02xln2-c-python-0.1/lib/python3.9/site-packages .
```

This is because the source folder already contained files generated by
autotools, and with the `--with-source` transformation, Guix takes all files
from the source folder, and doesn't need to run `autogen.sh` again, so the
`Makefile.in` wasn't up-to-date with the content of the `Makefile.am` we just
modified. Let's remove all files autotools generated:
```sh
rm -rf autom4te.cache aclocal.m4 build* configure Makefile.in python/Makefile.in src/config.h.in src/Makefile.in
```

New error:
```
phase `unpack' succeeded after 0.0 seconds
starting phase `bootstrap'
running './autogen.sh'
patch-shebang: ./autogen.sh: changing `/bin/bash' to `/gnu/store/vx6vfbmmazvfi7vp8xyjn2mcyylvw9gn-bash-minimal-5.1.8/bin/bash'
./autogen.sh: line 2: autoreconf: command not found
error: in phase 'bootstrap': uncaught exception:
%exception #<&invoke-error program: "./autogen.sh" arguments: () exit-status: 127 term-signal: #f stop-signal: #f>
phase `bootstrap' failed after 0.0 seconds
```

Ok, we have to provide `autoconf` and `automake` as build-dependencies
(although we specified it's a GNU build system...):
```scheme
(native-inputs (list python python-pip python-setuptools python-wheel autoconf automake))
```

New error:
```
configure.ac:1: error: Autoconf version 2.71 or higher is required
```
Although version Autoconf 2.71 is available in Guix:
```
% guix show autoconf | grep -B 1 version
name: autoconf
version: 2.71
--
name: autoconf
version: 2.69
--
name: autoconf
version: 2.68
--
name: autoconf
version: 2.64
--
name: autoconf
version: 2.13
```

By putting
```sh
autoconf --version
```
at the beginning of the `autogen.sh` file, we learn the version 2.69 of
autoconf is used. We are using nothing specific to the version 2.71 of
autoconf, let's lower our requirement:
```sh
AC_PREREQ([2.69])
```

We can finally get back to the "original" error we were trying to fix. The
verbose output of `pip` doesn't really complete the error message: Python can't
find `setuptools` and it can't connect to `pypi.org`. Not being able to connect
to an external server seems normal for the Guix packaging process. However, we
provided `python-setuptools` as a build-dependency... It turns out `pip` by
default isolates the package building and creates itself an environment
containing all packages it needs, ignoring all packages already available on
the system. To get these packages, it tries to download them from `pypi.org`,
hence the connection error. We can disable this isolation:
```sh
$(PYTHON) -m pip install --upgrade --no-build-isolation --target $(pythondir) .
```


We go further, but new error:
```
ValueError: ZIP does not support timestamps before 1980
Building wheel for ph-prog (PEP 517): finished with status 'error'
ERROR: Failed building wheel for ph-prog
```

Probably for reproducibility, timestamps are set to a constant value, which ZIP
doesn't like... I found the solution
[here](https://github.com/NixOS/nixpkgs/issues/270#issuecomment-467583872): we
can define an environment variable telling what is the constant timestamp:
```scheme
(arguments '(
  #:out-of-source? #t
  #:phases (modify-phases %standard-phases
      (add-before 'install 'pre-install
        (lambda _ (setenv "SOURCE_DATE_EPOCH" "315532800") #t)))))
```

Now the build succeeds, even if there is still a `pip` error:
```
1 location(s) to search for versions of pip:
* https://pypi.org/simple/pip/
Fetching project page and analyzing links: https://pypi.org/simple/pip/
Getting page https://pypi.org/simple/pip/
Found index url https://pypi.org/simple
Starting new HTTPS connection (1): pypi.org:443
Could not fetch URL https://pypi.org/simple/pip/: connection error: HTTPSConnectionPool(host='pypi.org', port=443): Max retries exceeded with url: /simple/pip/ (Caused by NewConnectionError('<pip._vendor.urllib3.connection.HTTPSConnection object at 0x7ffff5b0b4c0>: Failed to establish a new connection: [Errno -2] Name or service not known')) - skipping
```

`pip` seems to try to check if a new version of itself is available. We can
disable this behaviour:
```sh
$(PYTHON) -m pip install --upgrade --no-build-isolation --disable-pip-version-check --target $(pythondir) .
```


Time to test if the package works well in a Guix environment:
```
% guix shell -L . c-python --with-source=c-python=$(pwd)
% ph_prog | ph_prog_analyze
Traceback (most recent call last):
  File "/gnu/store/nmdsjm2kjq3s8qyaxxyzax734xkvb5yp-profile/bin/ph_prog_analyze", line 4, in <module>
    from ph_prog import OutputAnalyzer
ModuleNotFoundError: No module named 'ph_prog'
```

Indeed, even if we are in a Guix environment, the Python used for the
`ph_prog_analyze` script is the one from the host system:
```
% which python3
/usr/bin/python3
```

Let's use a _pure_ environment:
```
% guix shell -L . --pure c-python coreutils --with-source=c-python=$(pwd)
% which python3
python3 not found
```

Let's use a pure environment with Python available:
```
% guix shell -L . --pure c-python coreutils python --with-source=c-python=$(pwd)
% which python3
/gnu/store/pv32fzaxdi33vdc5z29hzy6g717csarp-profile/bin/python3
% ph_prog | ph_prog_analyze
Output: [1, 2, 3] sum=6
```

We will always need Python when using our package, so add it as a
`propagated-inputs`:
```scheme
(propagated-inputs (list python))
```
We don't need the environment to be pure anymore:
```
% guix shell -L . c-python --with-source=c-python=$(pwd)
% which ph_prog
/gnu/store/kzczh2v3a4r8g5srgs8yvhkzl8ipyzjw-profile/bin/ph_prog
% which ph_prog_analyze
/gnu/store/kzczh2v3a4r8g5srgs8yvhkzl8ipyzjw-profile/bin/ph_prog_analyze
% which python3
/gnu/store/kzczh2v3a4r8g5srgs8yvhkzl8ipyzjw-profile/bin/python3
% ph_prog | ph_prog_analyze
Output: [1, 2, 3] sum=6
```

The package is ready to be used within Guix!
