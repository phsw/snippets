(define-module (guix c-python)
    #:use-module (guix build-system gnu)
    #:use-module (guix download)
    #:use-module (guix licenses)
    #:use-module (guix packages)
    #:use-module (gnu packages autotools)
    #:use-module (gnu packages python)
    #:use-module (gnu packages python-build)
    #:use-module (gnu packages python-xyz))

(define-public c-python
    (package
        (name "c-python")
        (version "0.1")
	(source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnu/hello/hello-2.10.tar.gz"))
              (sha256
               (base32
                "0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i"))))
        (build-system gnu-build-system)
	(arguments
	  '(#:out-of-source? #t
	  #:phases (modify-phases %standard-phases
                  (add-before 'install 'pre-install
			      (lambda _ (setenv "SOURCE_DATE_EPOCH" "315532800") #t)))))
	(propagated-inputs (list python))
	(native-inputs (list python python-pip python-setuptools python-wheel autoconf automake))
	(synopsis "Hello, GNU world: An example GNU package")
	(description "Guess what GNU Hello prints!")
	(home-page "https://www.gnu.org/software/hello/")
	(license gpl3+)))
