class OutputAnalyzer:
    def __init__(self, value):
        self.values = list(map(int, value.strip().split()))

    def get_sum_values(self):
        return sum(self.values)
