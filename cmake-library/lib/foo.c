#include <stdio.h>
#include <mpi.h>

void foo(void)
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    printf("foo %d !!!\n", rank);
}
