// From https://stackoverflow.com/questions/1442116/how-to-get-the-date-and-time-values-in-a-c-program

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    time_t t = time(NULL);
    struct tm now_tm = *localtime(&t);
    printf("%d/%d/%d\n", now_tm.tm_mday, now_tm.tm_mon+1, now_tm.tm_year+1900);
    return EXIT_SUCCESS;
}
