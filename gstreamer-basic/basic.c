/* From https://gstreamer.freedesktop.org/documentation/application-development/highlevel/playback-components.html?gi-language=c */
#include <stdio.h>
#include <gst/gst.h>

/* Event/callback management from
 * https://gstreamer.freedesktop.org/documentation/tutorials/basic/toolkit-integration.html?gi-language=c
 * see also:
 * From https://gstreamer.freedesktop.org/documentation/tutorials/playback/playbin-usage.html?gi-language=c
 */

struct player_s
{
    GstState state;
    GstElement* playbin;
    gint64 duration;
    GMainLoop* loop;
};

static void callback_eos(GstBus* bus, GstMessage* msg, void* data)
{
    struct player_s* player = (struct player_s*) data;
    g_print("End-Of-Stream reached.\n");

    /* Set the pipeline to READY (which stops playback) */
    gst_element_set_state(player->playbin, GST_STATE_READY);

    g_main_loop_quit(player->loop);
}

static void callback_error(GstBus* bus, GstMessage* msg, void* data)
{
    GError* err;
    gchar* debug_info;
    struct player_s* player = (struct player_s*) data;

    gst_message_parse_error(msg, &err, &debug_info);
    g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME(msg->src), err->message);
    g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none");
    g_clear_error(&err);
    g_free(debug_info);

    /* Set the pipeline to READY (which stops playback) */
    gst_element_set_state(player->playbin, GST_STATE_READY);
}

static void callback_state_changed(GstBus* bus, GstMessage* msg, void* data)
{
    struct player_s* player = (struct player_s*) data;
    GstState old_state, new_state, pending_state;

    gst_message_parse_state_changed(msg, &old_state, &new_state, &pending_state);

    if (GST_MESSAGE_SRC(msg) == GST_OBJECT(player->playbin))
    {
        g_print("State changed to %s\n", gst_element_state_get_name(new_state));
        player->state = new_state;
    }
}

static gboolean callback_timer(void* data)
{
    struct player_s* player = (struct player_s*) data;
    gint64 current = -1;

    if (player->state < GST_STATE_PAUSED)
    {
        return TRUE;
    }

    if (!GST_CLOCK_TIME_IS_VALID(player->duration))
    {
        if (!gst_element_query_duration(player->playbin, GST_FORMAT_TIME, &player->duration))
        {
            g_printerr("Could not query current duration.\n");
        }
    }

    if (gst_element_query_position(player->playbin, GST_FORMAT_TIME, &current))
    {
        g_print("%" GST_TIME_FORMAT " / %" GST_TIME_FORMAT "\n", GST_TIME_ARGS(current), GST_TIME_ARGS(player->duration));
    }

    return TRUE;
}

gint main(gint argc, gchar *argv[])
{
  struct player_s player;
  GstBus *bus;
  char filepath[255];
  int ret;

  if (argc != 2)
  {
    g_print("Usage: %s <URI>\n", argv[0]);
    return -1;
  }

  player.duration = GST_CLOCK_TIME_NONE;

  gst_init(&argc, &argv);
  player.loop = g_main_loop_new(NULL, FALSE);

  player.playbin = gst_element_factory_make("playbin", "playbin");
  if (player.playbin == NULL)
  {
      g_printerr("Playbin creation failed.\n");
      return -1;
  }

  sprintf(filepath, "file://%s", argv[1]);
  g_print("uri of playbin is: %s\n", filepath);

  g_object_set(player.playbin, "uri", filepath, NULL);

  bus = gst_element_get_bus(player.playbin);
  gst_bus_add_signal_watch(bus);
  g_signal_connect(G_OBJECT(bus), "message::error", (GCallback) callback_error, &player);
  g_signal_connect(G_OBJECT(bus), "message::eos", (GCallback) callback_eos, &player);
  g_signal_connect(G_OBJECT(bus), "message::state-changed", (GCallback) callback_state_changed, &player);
  gst_object_unref(bus);

  ret = gst_element_set_state(player.playbin, GST_STATE_PLAYING);
  if (ret == GST_STATE_CHANGE_FAILURE)
  {
      g_printerr("Unable to set the pipeline to the playing state.\n");
      gst_object_unref(player.playbin);
      return -1;
  }

  g_timeout_add_seconds(1, (GSourceFunc) callback_timer, &player);

  g_main_loop_run(player.loop);

  gst_element_set_state(player.playbin, GST_STATE_NULL);
  gst_object_unref(player.playbin);

  return 0;
}
