## On Debian

Install packages `libgstreamer1.0-dev` and `libgstreamer-plugins-bad1.0-dev` (for `gstplayer`)


## On MacOs

With Homebrew, install `gstreamer`, `gstreamer-plugins-good` and `gstreamer-plugins-bad`.

- `basic`: add `/usr/local/opt/gstreamer/lib/pkgconfig` to the `PKG_CONFIG_PATH` variable
- `gstplayer`: add `/usr/local/opt/gst-plugins-bad/lib/pkgconfig` to the `PKG_CONFIG_PATH` variable


## On Windows

With MSYS2, install `mingw-w64-x86_64-gstreamer`, `mingw-w64-x86_64-gst-plugins-good` and `mingw-w64-x86_64-gst-plugins-bad` (for `gstplayer`)


## Building

- `basic`:
    ```bash
    gcc main.c `pkg-config --cflags --libs gstreamer-1.0`
    ```
- `gstplayer`:
    ```bash
    gcc gstplayer.c `pkg-config --cflags --libs gstreamer-player-1.0`
    ```
