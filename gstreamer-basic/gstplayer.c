/* Documentation: https://gstreamer.freedesktop.org/documentation/player/gstplayer.html?gi-language=c
 * An example: https://github.com/GStreamer/gst-examples/blob/master/playback/player/gst-play/gst-play.c
 */
#include <stdio.h>
#include <gst/gst.h>
#include <gst/player/player.h>


struct player_s
{
    GstPlayerState state;
    GstClockTime duration;
    GMainLoop* loop;
    GstPlayer* gst_player;
};


static gboolean callback_timer(void* data)
{
    struct player_s* player = (struct player_s*) data;
    GstClockTime position;

    if (!GST_CLOCK_TIME_IS_VALID(player->duration))
    {
        player->duration = gst_player_get_duration(player->gst_player);
    }

    if (player->state < GST_PLAYER_STATE_PAUSED)
    {
        return TRUE;
    }

    position = gst_player_get_position(player->gst_player);
    g_print("%" GST_TIME_FORMAT " / %" GST_TIME_FORMAT "\n", GST_TIME_ARGS(position), GST_TIME_ARGS(player->duration));

    return TRUE;
}

static void callback_state_changed(GstPlayer* gst_player, GstPlayerState player_state, void* data)
{
    struct player_s* player = (struct player_s*) data;
    player->state = player_state;

    printf("State changed.\n");
}

static void callback_eos(GstPlayer* gst_player, void* data)
{
    struct player_s* player = (struct player_s*) data;

    g_main_loop_quit(player->loop);
}

static void callback_duration_changed(GstPlayer* gst_player, guint64 duration, void* data)
{
    printf("duration changed: %" GST_TIME_FORMAT " (%llu ns)\n", GST_TIME_ARGS(duration), duration);
}

gint main(gint argc, gchar *argv[])
{
  struct player_s player;
  char filepath[255];
  int ret;

  if (argc != 2)
  {
    g_print("Usage: %s <URI>\n", argv[0]);
    return -1;
  }

  player.duration = GST_CLOCK_TIME_NONE;

  gst_init(&argc, &argv);
  player.gst_player = gst_player_new(NULL, NULL);
  player.loop = g_main_loop_new(NULL, FALSE);

  sprintf(filepath, "file://%s", argv[1]);
  g_print("uri of playbin is: %s\n", filepath);

  gst_player_set_uri(player.gst_player, filepath);

  g_signal_connect(G_OBJECT(player.gst_player), "state-changed", G_CALLBACK(callback_state_changed), &player);
  g_signal_connect(G_OBJECT(player.gst_player), "end-of-stream", G_CALLBACK(callback_eos), &player);
  g_signal_connect(G_OBJECT(player.gst_player), "duration-changed", G_CALLBACK(callback_duration_changed), &player);

  gst_player_play(player.gst_player);

  g_timeout_add_seconds(1, (GSourceFunc) callback_timer, &player);

  g_main_loop_run(player.loop);

  return 0;
}
