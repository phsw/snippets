#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <hwloc.h>

#define SIZE 10

int main()
{
	int* memory = malloc(SIZE * sizeof(int));
	hwloc_topology_t topology;

	hwloc_topology_init(&topology);
	hwloc_topology_load(topology);

	/* Write in the buffer to really allocate the memory: */
	for (int i = 0; i < SIZE; i++)
	{
		memory[i] = i;
	}

	printf("Buffer was allocated at address %p\n", memory);
	printf("memory[0] = %d; memory[%d] = %d\n", memory[0], SIZE-1, memory[SIZE-1]);

	hwloc_bitmap_t set = hwloc_bitmap_alloc();
	int ret = hwloc_get_area_memlocation(topology, memory, SIZE*sizeof(int), set, 0);
	if (ret != 0)
	{
		perror("hwloc_get_area_memlocation");
		hwloc_bitmap_free(set);
		hwloc_topology_destroy(topology);
		free(memory);
		return EXIT_FAILURE;
	}

	int id = hwloc_bitmap_first(set);
	printf("This memory is on NUMA node %d (physical numbering)\n", id);
	hwloc_obj_t numa_node = hwloc_get_numanode_obj_by_os_index(topology, id);
	if (numa_node)
	{
		printf("This memory is on NUMA node %d (logical numbering)\n", numa_node->logical_index);
	}
	else
	{
		printf("Can't find the matching NUMA node\n");
	}

	hwloc_bitmap_free(set);
	hwloc_topology_destroy(topology);
	free(memory);
	return EXIT_SUCCESS;
}
