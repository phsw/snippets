## How to find on which NUMA node the memory is allocated ?

Either with `move_pages` (requires the package `libnuma-dev`) or with hwloc.

See [this](https://stackoverflow.com/questions/7986903/can-i-get-the-numa-node-from-a-pointer-address-in-c-on-linux/8015480#8015480).

To test:
```bash
hwloc-bind --membind node:0 ./hwloc
```
