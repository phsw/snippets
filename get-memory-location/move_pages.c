#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <numaif.h>

#define SIZE 10

int main()
{
	int* memory = malloc(SIZE * sizeof(int));

	/* Write in the buffer to really allocate the memory: */
	for (int i = 0; i < SIZE; i++)
	{
		memory[i] = i;
	}

	printf("Buffer was allocated at address %p\n", memory);
	printf("memory[0] = %d; memory[%d] = %d\n", memory[0], SIZE-1, memory[SIZE-1]);

	int status;
	long ret = move_pages(0, 1, (void**) &memory, NULL, &status, 0);
	if (ret != 0)
	{
		perror("move_pages");
		free(memory);
		return EXIT_FAILURE;
	}

	printf("This memory is on NUMA node %d (physical numbering)\n", status);

	free(memory);
	return EXIT_SUCCESS;
}
