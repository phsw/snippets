# Generate the weve form of the music.wav file

# Some information about wave generation:
# https://stackoverflow.com/questions/26663494/algorithm-to-draw-waveform-from-audio

import wave, struct
from matplotlib import pyplot as plt


FRAME_SIZE = 1000


def compute_rms(l):
    s = 0
    for e in l:
        s += e ** 2

    return (s / len(l)) ** 0.5


def preprocess(l):
    return list(map(lambda e: e / 2**15, l))


waveFile = wave.open('music.wav', 'r')

length = waveFile.getnframes()
amp_max = []
amp_min = []
rms = []

print(waveFile.getparams())

for i in range(0, length // FRAME_SIZE):
    waveData = waveFile.readframes(FRAME_SIZE)
    data = preprocess(struct.unpack("<" + str(FRAME_SIZE) + "h", waveData))
    amp_max.append(max(data))
    amp_min.append(min(data))
    rms.append(compute_rms(data))


plt.fill_between(range(len(amp_min)), amp_min, amp_max, color="#347deb")
plt.fill_between(range(len(rms)), list(map(lambda e: -e, rms)), rms, color="#20529e")
plt.show()

waveFile.close()
