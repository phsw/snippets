import re

def convert_rec_file(filename):
    """
    Function to convert rec file to list of dict.
    Keys are lower-cased.
    """
    lines = []
    item = dict()

    with open(filename, "r") as f:
        for l in f.readlines():
            if l == "\n":
                lines.append(item)
                item = dict()
            else:
                ls = l.split(":")
                key = ls[0].lower()
                value = ls[1].strip()

                if key in item:
                    print("Warning: duplicated key '" + key + "'")
                else:
                    if re.match('^\d+$', value) != None:
                        item[key] = int(value)
                    elif re.match("^\d+\.\d+$", value) != None:
                        item[key] = float(value)
                    else:
                        item[key] = value

    return lines
