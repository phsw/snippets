#include <stdio.h>
#include <stdlib.h>
#include <x86intrin.h>

#define NB_FLOATS 32
#define ALIGN 16

static float* buffer;
static int nb_floats = NB_FLOATS;

static void do_non_temporal()
{
	__m128 pi = _mm_set_ps(3.14, 3.14, 3.14, 3.14);
	int i = 0;

	posix_memalign((void**) &buffer, ALIGN, nb_floats*sizeof(float));

	for (i = 0; i < nb_floats; i += (ALIGN/sizeof(float)))
	{
		_mm_stream_ps(buffer + i, pi);
	}
}

static void do_memset()
{
	int i = 0;

	buffer = malloc(nb_floats*sizeof(float));

	for (i = 0; i < nb_floats; i++)
	{
		buffer[i] = 3.14;
	}
}

int main(int argc, char* argv[])
{
	int i = 0;
	int non_temporal = 1;
	int display_buffer = 1;

	if (argc >= 2)
	{
		nb_floats = atoi(argv[1]);
	}
	if (argc >= 3)
	{
		non_temporal = atoi(argv[2]);
	}
	if (argc >= 4)
	{
		display_buffer = atoi(argv[3]);
	}

	printf("# Will use %d floats\n", nb_floats);
	printf("# Will %suse non temporal instructions\n", non_temporal ? "" : "not ");
	printf("# Will %sdisplay buffer\n", display_buffer ? "" : "not ");

	if (non_temporal)
	{
		do_non_temporal();
	}
	else
	{
		do_memset();
	}

	if (display_buffer)
	{
		for (i = 0; i < nb_floats; i++)
		{
			printf("buffer[%d] = %f\n", i, buffer[i]);
		}
	}

	free(buffer);
	return EXIT_SUCCESS;
}
