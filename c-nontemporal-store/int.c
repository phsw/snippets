#include <stdio.h>
#include <stdlib.h>
#include <x86intrin.h>

#define NB_INTS 32

int main()
{
	int i = 0;
	int* buffer = malloc(NB_INTS * sizeof(int));

	for (i = 0; i < NB_INTS; i++)
	{
		_mm_stream_si32(buffer + i, 42);
	}

	for (i = 0; i < NB_INTS; i++)
	{
		printf("buffer[%d] = %d\n", i, buffer[i]);
	}

	free(buffer);
	return EXIT_SUCCESS;
}
