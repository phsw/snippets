# Non-temporal stores

SSE instructions bypassing the last level cache, writing directly to the main
memory.

To know an equivalence on ARM: https://github.com/DLTcollab/sse2neon.
