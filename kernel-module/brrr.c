#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#define DEV_NAME "brrr"

MODULE_DESCRIPTION("BRRRR");
MODULE_AUTHOR("CFS");
MODULE_LICENSE("BRRR license");

static char brrr[] = "brrrrrrrrrrrrrrrrrrrr\n";
static int brrrlen;
static int major;

static ssize_t brrr_read(struct file*, char*, size_t, loff_t*);

static struct file_operations fops = {
	.read = brrr_read
};

static ssize_t brrr_read(struct file* fp, char* buf, size_t len, loff_t* off)
{
	// TODO: min(len, brrrlen)
	if (copy_to_user(buf, brrr, brrrlen) != 0)
	{
		printk("Error in read, copy to user\n");
		return -EFAULT;
	}
	return brrrlen;
}

static int brrr_init(void)
{
	major = register_chrdev(0, DEV_NAME, &fops);
	if (major < 0)
	{
		printk("Ooops\n");
		return major;
	}
	brrrlen = strlen(brrr);
	return 0;
}

static void brrr_exit(void)
{
	if (major >= 0)
	{
		unregister_chrdev(major, DEV_NAME);
	}
}

module_init(brrr_init);
module_exit(brrr_exit);
