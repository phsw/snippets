# Simple example of Linux module

A simple Linux module creating a character devices producing `brrrrr` content.


## Building

```sh
sudo apt install build-essential linux-headers-amd64
make -C /lib/modules/$(uname -r)/build M=$(pwd)
```


## Using

Messages printed with `printk` appears in `dmesg`.

Getting informations about the built module:
```sh
sudo modinfo brrr.ko
```

Loading the module:
```sh
sudo insmod brrr.ko
```

Check the module is loaded:
```sh
lsmod | grep brrr
grep brrr /proc/modules
```

Create the device:
```sh
grep brr /proc/devices # the number is the device major number, required in the next command
sudo mknod /dev/brrr c 248 0
```

Use the device:
```sh
cat /dev/brrrr
```
