#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define NB_THREADS 5
#define NB_BARRIERS 3

pthread_barrier_t barrier;

void* thread_fun(void* arg)
{
	int* thread_id = (int*) arg;

	for (int i = 0; i < NB_BARRIERS; i++)
	{
		printf("In thread %d\n", *thread_id);
		pthread_barrier_wait(&barrier);
		pthread_barrier_wait(&barrier); // double barrier to wait main prints its text
	}
}

int main(int argc, char* argv[])
{
	pthread_t threads[NB_THREADS];
	int threads_id[NB_THREADS];

	pthread_barrier_init(&barrier, NULL, NB_THREADS+1);

	printf("Starting %d threads\n", NB_THREADS);

	for (int t = 0; t < NB_THREADS; t++)
	{
		threads_id[t] = t;
		pthread_create(&threads[t], NULL, thread_fun, &threads_id[t]);
	}

	for (int i = 0; i < NB_BARRIERS; i++)
	{
		pthread_barrier_wait(&barrier);
		printf("main: after barrier\n");
		pthread_barrier_wait(&barrier);
	}

	printf("main: before thread join\n");

	for (int t = 0; t < NB_THREADS; t++)
	{
		pthread_join(threads[t], NULL);
	}

	pthread_barrier_destroy(&barrier);

	return EXIT_SUCCESS;
}
