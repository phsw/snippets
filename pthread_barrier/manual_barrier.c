/* From
 * * http://byronlai.com/jekyll/update/2015/12/26/barrier.html
 * * https://blog.albertarmea.com/post/47089939939/using-pthreadbarrier-on-mac-os-x
 */
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define NB_BARRIERS 3

struct barrier_s {
	pthread_mutex_t mutex;
	pthread_cond_t condition_variable;
	int threads_required;
	int threads_left;
	unsigned int cycle;
};

int barrier_init(struct barrier_s* barrier, void *attr, int count)
{
	barrier->threads_required = count;
	barrier->threads_left = count;
	barrier->cycle = 0;
	pthread_mutex_init(&barrier->mutex, NULL);
	pthread_cond_init(&barrier->condition_variable, NULL);
	return 0;
}

int barrier_wait(struct barrier_s* barrier)
{
	pthread_mutex_lock(&barrier->mutex);

	if (--barrier->threads_left == 0) {
		barrier->cycle++;
		barrier->threads_left = barrier->threads_required;

		pthread_cond_broadcast(&barrier->condition_variable);
		pthread_mutex_unlock(&barrier->mutex);

		return 1;
	} else {
		unsigned int cycle = barrier->cycle;

		while (cycle == barrier->cycle)
			pthread_cond_wait(&barrier->condition_variable, &barrier->mutex);

		pthread_mutex_unlock(&barrier->mutex);
		return 0;
	}
}

int barrier_destroy(struct barrier_s* barrier)
{
	pthread_mutex_destroy(&barrier->mutex);
	pthread_cond_destroy(&barrier->condition_variable);
}

static struct barrier_s barrier;

void* thread_fun(void* arg)
{
	int* thread_id = (int*) arg;

	for (int i = 0; i < NB_BARRIERS; i++)
	{
		printf("2\tIn thread %d\n", *thread_id);
		barrier_wait(&barrier);
		barrier_wait(&barrier); // double barrier to wait main prints its text
	}
}

int main(int argc, char* argv[])
{
	pthread_t thread;
	int thread_id = 1;

	barrier_init(&barrier, NULL, 2);

	printf("1\tStarting 1 thread\n");

	pthread_create(&thread, NULL, thread_fun, &thread_id);

	for (int i = 0; i < NB_BARRIERS; i++)
	{
		barrier_wait(&barrier);
		printf("3\tmain: after barrier\n");
		barrier_wait(&barrier);
	}

	printf("4\tmain: before thread join\n");

	pthread_join(thread, NULL);

	barrier_destroy(&barrier);

	return EXIT_SUCCESS;
}
