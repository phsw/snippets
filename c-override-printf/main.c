#include <stdio.h>
#include <stdlib.h>

#define MY_PRINTF(msg, ...) do { printf(msg, ## __VA_ARGS__); } while(0)

int main()
{
	int number = 123;

	MY_PRINTF("abc %d\n", number);

	return EXIT_SUCCESS;
}
