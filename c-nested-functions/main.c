#include <stdio.h>
#include <stdlib.h>

int main()
{
    int square (int z) { return z * z; }

    for (int i = 0; i < 4; i++)
    {
        printf("%d^2 = %d\n", i, square(i));
    }

    return EXIT_SUCCESS;
}
