# Example of C source code using nested functions

Works if built with a GNU compiler, since it is a [GNU C
extension](https://gcc.gnu.org/onlinedocs/gcc/Nested-Functions.html).

To prevent it from working even with GCC, use the `-pedantic` option (it will
just trigger a warning, use `-pedantic-errors`, see
[documentation](https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html)).

Does not built with other compiler (tested with clang).
