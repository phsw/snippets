# Implementation of P^2 algorithm

This script contains an implementation of the [P^2
algorithm](https://dl.acm.org/doi/10.1145/4372.4378) to get percentiles of set
of data, without storing the data. It compares the distribution obtained with
this algorithm and with the knowledge of all data samples.

The class implementing the P^2 algorithms comes from this [GitHub
repository](https://github.com/rfrenoy/psquare).

The original paper is the following:
> Raj Jain and Imrich Chlamtac. 1985. The P2 algorithm for dynamic calculation
> of quantiles and histograms without storing observations. Commun. ACM 28, 10
> (Oct. 1985), 1076–1085. https://doi.org/10.1145/4372.4378

The P^2 algorithm is also implemented in the
[GSL](https://www.gnu.org/software/gsl/doc/html/rstat.html#quantiles).
