def get_slices(x, slices):
    current_slice = 0
    s = [[]]
    for i in range(len(x)):
        if current_slice < len(slices) and x[i] > slices[current_slice]:
            s.append([])
            s[-1].append(s[-2][-1])
            current_slice += 1
        s[-1].append(x[i])
    return s

assert(get_slices(list(range(20)), [8, 13]) == [[0, 1, 2, 3, 4, 5, 6, 7, 8], [8, 9, 10, 11, 12, 13], [13, 14, 15, 16, 17, 18, 19]])
