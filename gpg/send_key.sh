#!/bin/bash

if [ $# -eq 1 ]
then
    key_id=$1
else
    echo "You have to provide the key ID"
    exit 1
fi

echo "Sending key $key_id..."

servers="keys.openpgp.org keyserver.ubuntu.com pgpkeys.eu pgp.surf.nl the.earth.li"
for server in $servers; do
    echo "Sending to $server"
    gpg --keyserver $server --send-key $key_id
done
