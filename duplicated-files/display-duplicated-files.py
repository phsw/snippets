# Needs Python 3.5

import glob
import os

DEPTH = 1  # number of element at the end of the filepath to compare

all_files = glob.glob("**/*", recursive=True)

files = dict()  # filename => [paths, ...]

for f in all_files:
    if os.path.isfile(f):
        filename = "/".join(f.split('/')[-DEPTH:])

        if filename not in files:
            files[filename] = [f]
        else:
            files[filename].append(f)

nb = 0
for filename, paths in files.items():
    if len(paths) > 1:
        nb += 1
        print(filename)
        for p in paths:
            print("\t" + p)

print(nb)
